# Note: to reload the package once it has been reinstalled
#    you can use reload from devtools package, ex:
#    reload("frenchLandscape");source("script_RPG.R")

library("frenchLandscape")
library("rgeos")
library("plotGoogleMaps")
library("data.table")
library("sp")

inDir <- system.file("extdata",package="RPG")

# we leave 2006 aside for now as it is messed up (dixit ODR) + file names are messed up
toLaunch <- factory(function(){
               for(year in 2007:2014){
                   for(dep in 1:95){
                       VerificationRPG(year,dep,inDir)
                   }
               }
})
out <- toLaunch()
save(out,file=file.path(inDir,"out_from_verification.rda"))
