
########################
# 
# Script: setparam_generalModels.R  
#
# Purpose: give the graphic vizualisation for models synthesis 
# Details: graphic under matrix format (as Nathan) 
#
# Author: Thomas Delaune (thomas.delaune.inra@gmail.com)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################
## easy debugging options
# options(error=browser)
# options(warn=2)

ExpectedOutputs <- list("Reference",
                        "Modelisation_nolog", "Modelisation_PA", 
                        "Response_Min", "Response_Max", 
                        "Variable_NoRot", "Variable_NoExtraLand", 
                        "Variable_No10kPCA4", "Variable_No10kPCA16", 
                        "Control_NosegArv", "Control_NoGrandesRegions","Control_NoControl" ) 

RunAllSimu <- function(exp_output){ 

    #Reference parameters, applicable for models if no conditional change
    modelName <-"LassoLogNormBin" # binomial model with Log normalized X
    metricModel <- "nAboveMedMoy" # y is the number of obs above the median of observations
    metricsGroup <- "iRPGSame,iRPGPrec,iPrairiesSame,iRPGproxRot3,iRPGproxPrairie3,iBDTOPOGetBoisHaie,iBDTOPOGetLandeIGN"
    XtoPrecise <- "XLogNormGlm"
    scales <- "200|1000|5000|10000"
    factorNames <- c("campagne_grandeRegion", "segArvSysteme")

    if(exp_output == "Reference"){
        exp_output <-"Reference"
    } 

    if(exp_output == "Modelisation_nolog"){
        exp_output <-"Modelisation_nolog"
        modelName <-"LassoLogNormBin" # binomial model with Log normalized X
        # metricModel <- "nAboveMedMoy" # y is the number of obs above the median of observations
        # metricsGroup <- "iRPGSame,iRPGPrec,iPrairiesSame,iRPGproxRot3,iRPGproxPrairie3,iBDTOPOGetBoisHaie,iBDTOPOGetLandeIGN"
        XtoPrecise <- "XnormGlm"
        # scales <- "200|1000|5000|10000"
        # factorNames <- c("campagne_grandeRegion", "segArvSysteme")
    } 

    if(exp_output == "Modelisation_PA"){
        exp_output <-"Modelisation_PA"
        modelName <-"LassoLogNormPA" # binomial model with Log normalized X
        # metricModel <- "nAboveMedMoy" # y is the number of obs above the median of observations
        # metricsGroup <- "iRPGSame,iRPGPrec,iPrairiesSame,iRPGproxRot3,iRPGproxPrairie3,iBDTOPOGetBoisHaie,iBDTOPOGetLandeIGN"
        # XtoPrecise <- "XLogNormGlm"
        # scales <- "200|1000|5000|10000"
        # factorNames <- c("campagne_grandeRegion", "segArvSysteme")
    } 


    if(exp_output == "Response_Max"){
        exp_output <-"Response_Max"
        # modelName <-"LassoLogNormBin" # binomial model with Log normalized X
        metricModel <- "nAboveQhighMax" # y is the number of obs above the median of observations
        # metricsGroup <- "iRPGSame,iRPGPrec,iPrairiesSame,iRPGproxRot3,iRPGproxPrairie3,iBDTOPOGetBoisHaie,iBDTOPOGetLandeIGN"
        # XtoPrecise <- "XLogNormGlm"
        # scales <- "200|1000|5000|10000"
        # factorNames <- c("campagne_grandeRegion", "segArvSysteme")
    } 

    if(exp_output == "Response_Min"){
        exp_output <-"Response_Min"
        # modelName <-"LassoLogNormBin" # binomial model with Log normalized X
        metricModel <- "nAboveMedMin" # y is the number of obs above the median of observations
        # metricsGroup <- "iRPGSame,iRPGPrec,iPrairiesSame,iRPGproxRot3,iRPGproxPrairie3,iBDTOPOGetBoisHaie,iBDTOPOGetLandeIGN"
        # XtoPrecise <- "XLogNormGlm"
        # scales <- "200|1000|5000|10000"
        # factorNames <- c("campagne_grandeRegion", "segArvSysteme")
    }

    if(exp_output == "Variable_NoRot"){
        exp_output <-"Variable_NoRot"
        # modelName <-"LassoLogNormBin" # binomial model with Log normalized X
        # metricModel <- "nAboveMedMoy" # y is the number of obs above the median of observations
        metricsGroup <- "iRPGSame,iRPGPrec,iPrairiesSame,iBDTOPOGetBoisHaie,iBDTOPOGetLandeIGN"
        # XtoPrecise <- "XLogNormGlm"
        # scales <- "200|1000|5000|10000"
        # factorNames <- c("campagne_grandeRegion", "segArvSysteme")
    } 

    if(exp_output == "Variable_NoExtraLand"){ # add lignous
        exp_output <-"Variable_NoExtraLand"
        # modelName <-"LassoLogNormBin" # binomial model with Log normalized X
        # metricModel <- "nAboveMedMoy" # y is the number of obs above the median of observations
        metricsGroup <-  "iRPGSame,iRPGPrec,iPrairiesSame,iRPGproxRot3,iRPGproxPrairie3,iBDTOPOGetBoisHaie,iBDTOPOGetLandeIGN,iBDTOPOGetPVVIGN"   
        # XtoPrecise <- "XLogNormGlm"
        # scales <- "200|1000|5000|10000"
        # factorNames <- c("campagne_grandeRegion", "segArvSysteme")
    } 

    if(exp_output == "Variable_No10kPCA4"){
        exp_output <-"Variable_No10kPCA4"
        # modelName <-"LassoLogNormBin" # binomial model with Log normalized X
        # metricModel <- "nAboveMedMoy" # y is the number of obs above the median of observations
        metricsGroup <- "iRPGSame,iRPGPrec,iPrairiesSame,iRPGproxRot3,iRPGproxPrairie3,iBDTOPOGetBoisHaie,iBDTOPOGetLandeIGN,iPC_4"
        # XtoPrecise <- "XLogNormGlm"
        scales <- "200|1000|5000"
        # factorNames <- c("campagne_grandeRegion", "segArvSysteme")
    } 

    #if(exp_output == "Variable_No10kPCA16"){
    #    exp_output <-"Variable_No10kPCA16"
    #    # modelName <-"LassoLogNormBin" # binomial model with Log normalized X
    #    # metricModel <- "nAboveMedMoy" # y is the number of obs above the median of observations
    #    metricsGroup <- "iRPGSame,iRPGPrec,iPrairiesSame,iRPGproxRot3,iRPGproxPrairie3,iBDTOPOGetBoisHaie,iBDTOPOGetLandeIGN,iPC_16"
    #    # XtoPrecise <- "XLogNormGlm"
    #    scales <- "200|1000|5000"
    #    # factorNames <- c("campagne_grandeRegion", "segArvSysteme")
    #} 

    if(exp_output == "Control_NosegArv"){
        exp_output <-"Control_NosegArv"
        # modelName <-"LassoLogNormBin" # binomial model with Log normalized X
        # metricModel <- "nAboveMedMoy" # y is the number of obs above the median of observations
        # metricsGroup <- "iRPGSame,iRPGPrec,iPrairiesSame,iRPGproxRot3,iRPGproxPrairie3,iBDTOPOGetBoisHaie,iBDTOPOGetLandeIGN"
        # XtoPrecise <- "XLogNormGlm"
        # scales <- "200|1000|5000|10000"
        factorNames <- c("campagne_grandeRegion")
    } 

    if(exp_output == "Control_NoGrandesRegions"){
        exp_output <- "Control_NoGrandesRegions"
        # modelName <-"LassoLogNormBin" # binomial model with Log normalized X
        # metricModel <- "nAboveMedMoy" # y is the number of obs above the median of observations
        # metricsGroup <- "iRPGSame,iRPGPrec,iPrairiesSame,iRPGproxRot3,iRPGproxPrairie3,iBDTOPOGetBoisHaie,iBDTOPOGetLandeIGN"
        # XtoPrecise <- "XLogNormGlm"
        # scales <- "200|1000|5000|10000"
        factorNames <- "segArvSysteme"
    } 
    if(exp_output == "Control_NoControl"){
        exp_output <- "Control_NoControl"
        # modelName <-"LassoLogNormBin" # binomial model with Log normalized X
        # metricModel <- "nAboveMedMoy" # y is the number of obs above the median of observations
        # metricsGroup <- "iRPGSame,iRPGPrec,iPrairiesSame,iRPGproxRot3,iRPGproxPrairie3,iBDTOPOGetBoisHaie,iBDTOPOGetLandeIGN"
        # XtoPrecise <- "XLogNormGlm"
        # scales <- "200|1000|5000|10000"
        factorNames <- c()
    }

    cat("exp_output<-\"",exp_output,"\"","\n",
        "nameSimu<-\"",exp_output,"\"","\n",
        "modelName<-\"",modelName,"\"","\n",
        "metricModel<-\"",metricModel,"\"","\n",
        "metricsGroup<-\"",metricsGroup,"\"","\n",
        "XtoPrecise<-\"",XtoPrecise,"\"","\n",
        "scales<-\"",scales,"\"","\n",
        "factorNames<-",deparse(factorNames),"\n",
        file = "parameters_sampler.r", append = FALSE,sep="")

    # Run the simulation with oec_launch 
    #browser()
    sl <- "./sec_launch.sh generalModels.R"
    system(sl)
    cat("Simulation : \n", nameSimu, "\n")
}

lapply(ExpectedOutputs, RunAllSimu)


#source("makeMatrixSynthesis.R") 
