library("frenchLandscape") # Pour le NumTo3Char et bien d'autres choses
library("rgeos")

metrics <- list()
colza <- ExtLoad("bioagresseurs","culture/vigicultures_colza")
ble <- ExtLoad("bioagresseurs","culture/vigicultures_ble")
mais <- ExtLoad("bioagresseurs","culture/vigicultures_mais")

pointsRef <- mais # rbind(ble,colza,mais)
# pointsRef <- pointsRef[which(pointsRef$codeDepartement == 18),]
for(shapesSelector in c(19)){
    cat("shapesSelector:",shapesSelector,"\n")
    for(year in 2014){
        metrics[[as.character(year)]] <- ExtractMetrics(pointsRef,"RPG",year,shapesSelector=shapesSelector,
                                                        widths=c(200,1000,5000,10000),
                                                        vectShortNames=c("minDistance","count","perimeter","area"),
                                                        nCores=4,useCache=FALSE)
    }
}

