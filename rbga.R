library("genalg")
monitor <- function(obj,fileMon=".rbga.mon") {
    # plot the population
    xlim = c(obj$stringMin[1], obj$stringMax[1]);
    ylim = c(obj$stringMin[2], obj$stringMax[2]);
    plot(obj$population, xlim=xlim, ylim=ylim,
         xlab="pi", ylab="sqrt(50)");
    abline(v=pi)
    abline(h=sqrt(50))
    Sys.sleep(0.2)

    colnames(obj$population) <- paste0("V",seq(1:dim(obj$population)[2]))
    monitored <- cbind(iter=obj$iter,eval=obj$evaluations,obj$population)
    write.table(file=fileMon,monitored,
                col.names=obj$iter==1,row.names=FALSE,
                append=!obj$iter==1)
}
rbga.trace <- function(fileMon=".rbga.mon"){
    monitored <- read.table(fileMon,header=TRUE)
    nParam <- dim(monitored)[2]-2
    par(mfrow=rep(ceiling(sqrt(nParam)),2))
    iMin <- which.min(monitored$eval)
    cat("Best value:\n")
    print(monitored[iMin,])
    for(iParam in 1:nParam){
        plot(monitored[,iParam+2],ylab="eval")
        abline(h=monitored[iMin,iParam+2])
        # plot(monitored[,iParam+2],monitored[,2],ylab="eval")
    }
    return(invisible(monitored))
}
evaluate <- function(theta=c(),salutation) {
    cat(salutation)
    returnVal = NA;
    if (length(theta) == 2) {
        returnVal = abs(theta[1]-pi) + abs(theta[2]-sqrt(50));
    } else {
        stop("Expecting a chromosome of length 2!");
    }
    returnVal
}
ToBounded <- function(x,min,max){
    return(min+max/(1+exp(-x)))
}
ToReal <- function(x,min,max){
    return(-log(max/(x-min)-1))
}
x <- seq(-10,10,0.1)
y <- ToBounded(x,20,100)
# par(mfrow=c(1,3))
# plot(x,ToBounded(x,20,100))
# plot(y,ToReal(y,20,100))
# plot(x,ToReal(y,20,100))

rbga <- function (stringMin = c(), stringMax = c(), suggestions = NULL, 
    popSize = NULL, iters = 100, mutationChance = NA, elitism = NA, 
    monitorFunc = NULL, evalFunc = NULL, showSettings = FALSE, 
    verbose = FALSE,nCores=1,obj=NULL,...) 
{
    if (is.null(evalFunc)) {
        stop("A evaluation function must be provided. See the evalFunc parameter.")
    }
    # and change to specified
    vars <- length(stringMin)
    
    SetDefault <-function(paramName,defaultVal){
        currentVal <- get(paramName,envir=parent.frame())
        if(is.na(currentVal)){
            if(!is.null(obj[[paramName]])){
                val <- obj[[paramName]]
            }else{
                val <- defaultVal
            }
            assign(paramName,val,envir=parent.frame())
        }
    }
    SetDefault("popSize",200)
    SetDefault("elitism",floor(popSize/5))
    SetDefault("mutationChance",1/(vars + 1))

    # sanity check
    if (verbose) 
        cat("Testing the sanity of parameters...\n")
    if (length(stringMin) != length(stringMax)) {
        stop("The vectors stringMin and stringMax must be of equal length.")
    }
    if (popSize < 5) {
        stop("The population size must be at least 5.")
    }
    if (iters < 1) {
        stop("The number of iterations must be at least 1.")
    }
    if (!(elitism < popSize)) {
        stop("The population size must be greater than the elitism.")
    }
    if (showSettings) {
        if (verbose) 
            cat("The start conditions:\n")
        result = list(stringMin = stringMin, stringMax = stringMax, 
            suggestions = suggestions, popSize = popSize, iters = iters, 
            elitism = elitism, mutationChance = mutationChance)
        class(result) = "rbga"
        cat(summary(result))
    }
    else {
        if (verbose) 
            cat("Not showing GA settings...\n")
    }
    if (vars > 0) {
        if (!is.null(suggestions)) {
            if (verbose) 
                cat("Adding suggestions to first population...\n")
            population = matrix(nrow = popSize, ncol = vars)
            suggestionCount = dim(suggestions)[1]
            browser()
            for (i in 1:suggestionCount) {
                population[i, ] = suggestions[i, ]
            }
            if (verbose) 
                cat("Filling others with random values in the given domains...\n")
            for (var in 1:vars) {
                population[(suggestionCount + 1):popSize, var] = stringMin[var] + 
                  runif(popSize - suggestionCount) * (stringMax[var] - 
                    stringMin[var])
            }
        }
        else {
            if (verbose) 
                cat("Starting with random values in the given domains...\n")
            population = matrix(nrow = popSize, ncol = vars)
            for (var in 1:vars) {
                population[, var] = stringMin[var] + runif(popSize) * 
                  (stringMax[var] - stringMin[var])
            }
        }
        bestEvals = rep(NA, iters)
        meanEvals = rep(NA, iters)
        evalVals = rep(NA, popSize)
        for (iter in 1:iters) {
            if (verbose) 
                cat(paste("Starting iteration", iter, "\n"))
            if (verbose) 
                cat("Calculating evaluation values... ")
            if(nCores > 1){
                eachParallel <- function(x,population){
                    evalVal <- matrix(NA,nrow=length(x),ncol=1)
                    for (iObject in 1:length(x)) {
                        object <- x[iObject]
                        if (is.na(evalVals[object])) {
                            evalVal[iObject,] <- evalFunc(population[object,],...)
                            if (verbose) 
                                cat(".")
                        }else{
                            evalVal[iObject,] <- evalVals[object]
                        }
                    }
                    return(evalVal)
                }
                evalVals <- as.vector(Par(population,FUN=eachParallel,nCores=nCores))
            }else{
                for (object in 1:popSize) {
                    if (is.na(evalVals[object])) {
                        evalVals[object] = evalFunc(population[object, 
                                                    ],...)
                  if (verbose) 
                      cat(".")
                    }
                }
            }

            bestEvals[iter] = min(evalVals)
            meanEvals[iter] = mean(evalVals)
            if (verbose) 
                cat(" done.\n")
            if (!is.null(monitorFunc)) {
                if (verbose) 
                  cat("Sending current state to rgba.monitor()...\n")
                result = list(type = "floats chromosome", stringMin = stringMin, 
                  stringMax = stringMax, popSize = popSize, iter = iter, 
                  iters = iters, population = population, elitism = elitism, 
                  mutationChance = mutationChance, evaluations = evalVals, 
                  best = bestEvals, mean = meanEvals)
                class(result) = "rbga"
                monitorFunc(result)
            }
            if (iter < iters) {
                if (verbose) 
                  cat("Creating next generation...\n")
                newPopulation = matrix(nrow = popSize, ncol = vars)
                newEvalVals = rep(NA, popSize)
                if (verbose) 
                  cat("  sorting results...\n")
                sortedEvaluations = sort(evalVals, index = TRUE)
                sortedPopulation = matrix(population[sortedEvaluations$ix, 
                  ], ncol = vars)
                if (elitism > 0) {
                  if (verbose) 
                    cat("  applying elitism...\n")
                  newPopulation[1:elitism, ] = sortedPopulation[1:elitism, 
                    ]
                  newEvalVals[1:elitism] = sortedEvaluations$x[1:elitism]
                }
                if (vars > 1) {
                  if (verbose) 
                    cat("  applying crossover...\n")
                  for (child in (elitism + 1):popSize) {
                    parentProb = dnorm(1:popSize, mean = 0, sd = (popSize/3))
                    parentIDs = sample(1:popSize, 2, prob = parentProb)
                    parents = sortedPopulation[parentIDs, ]
                    crossOverPoint = sample(0:vars, 1)
                    if (crossOverPoint == 0) {
                      newPopulation[child, ] = parents[2, ]
                      newEvalVals[child] = sortedEvaluations$x[parentIDs[2]]
                    }
                    else if (crossOverPoint == vars) {
                      newPopulation[child, ] = parents[1, ]
                      newEvalVals[child] = sortedEvaluations$x[parentIDs[1]]
                    }
                    else {
                      newPopulation[child, ] = c(parents[1, ][1:crossOverPoint], 
                        parents[2, ][(crossOverPoint + 1):vars])
                    }
                  }
                }
                else {
                  if (verbose) 
                    cat("  cannot crossover (#vars=1), using new randoms...\n")
                  newPopulation[(elitism + 1):popSize, ] = sortedPopulation[sample(1:popSize, 
                    popSize - elitism), ]
                }
                population = newPopulation
                evalVals = newEvalVals
                if (mutationChance > 0) {
                  if (verbose) 
                    cat("  applying mutations... ")
                  mutationCount = 0
                  selectIndiv <- (elitism + 1):popSize
                  # mean between mean and sd of selected individuals, weighted by popSize

                  currentPrec <- rep(NA,length(vars))
                  currentPrecInReal <- currentPrec
                  for (var in 1:vars) {
                      currentPrec[var] <- mean(population[selectIndiv,var])/popSize + (1-1/popSize)*sd(population[selectIndiv,var])
                      varInReal <- ToReal(population[selectIndiv,var],stringMin[var],stringMax[var])
                      currentPrecInReal[var] <- abs(mean(varInReal)/popSize) + (1-1/popSize)*sd(varInReal)
                  }
                  for (object in selectIndiv) {
                    for (var in 1:vars) {
                      if (runif(1) < mutationChance) {
                        # dempeningFactor = (iters - iter)/iters
                        dempeningFactor <- 1
                        direction <- 1 # sample(c(-1, 1), 1)
                        # mutScale <- (stringMax[var] - stringMin[var])*0.1
                        mutationVal = rnorm(n=1,mean=0,sd=currentPrec[var])
                        mutation <- population[object, var] + 
                          direction * mutationVal * dempeningFactor

                        mutationInReal <- rnorm(n=1,
                                                mean=ToReal(population[object,var],
                                                            stringMin[var],stringMax[var]),
                                                sd=currentPrecInReal[var])
                        mutation <- ToBounded(mutationInReal,stringMin[var],stringMax[var])

                        if (mutation < stringMin[var]) 
                          mutation = stringMin[var] + runif(1) * 
                            (stringMax[var] - stringMin[var])
                        if (mutation > stringMax[var]) 
                          mutation = stringMin[var] + runif(1) * 
                            (stringMax[var] - stringMin[var])
                        population[object, var] = mutation
                        evalVals[object] = NA
                        mutationCount = mutationCount + 1
                      }
                    }
                  }
                  if (verbose) 
                    cat(paste(mutationCount, "mutations applied\n"))
                }
            }
        }
    }
    print(apply(population,2,mean))
    result = list(type = "floats chromosome", stringMin = stringMin, 
        stringMax = stringMax, popSize = popSize, iters = iters, 
        suggestions = suggestions, population = population, elitism = elitism, 
        mutationChance = mutationChance, evaluations = evalVals, 
        best = bestEvals, mean = meanEvals)
    class(result) = "rbga"
    return(result)
}
par(mfrow=c(1,1))
rbga.results <- rbga(c(0, 0), c(100, 100), monitorFunc=monitor,evalFunc=evaluate, verbose=TRUE,mutationChance=0.5,popSize=6,iters=10,elitism=2,salutation="hello")

dev.new()
mon <- rbga.trace()


# rbga.results <- rbga(c(0, 0), c(100, 100), monitorFunc=monitor,evalFunc=evaluate, verbose=TRUE,mutationChance=0.5,popSize=30,iters=10,elitism=10)
# library("GA")
# fitness <- function(x){
#     1/evaluate(x)
# }
# summary(ga("real-valued",fitness=fitness,min=c(0,0),max=c(100,100),popSize=12,elitism=2,maxiter=20))
# # => not impressive at all for small populations


