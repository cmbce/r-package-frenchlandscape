#==================================================
# Parameters
#==================================================
ElScale <- c(-0.95,-0.75,-0.5,-0.25,0,0.25,0.5,0.75,0.95) # seq(-4,4)
#==================================================
# Libraries and functions
#==================================================
library("frenchLandscape")
library("CompElExplorer")
source("statsFns.R")

#==================================================
# Load elicitation data
#==================================================
# Ryohei, please, do not, ever, use an absolute path in a script we share: 
# it will not work for me. GetDicoPath is done for that. If you are troubles with it
# we can speak about how it works and how to improve it but I don't want to deal with changing 
# the paths of files in scripts I pull
baseFolder <- file.path(GetDicoPath("bioagresseurs"))
load(file.path(baseFolder,"leverTableMal.rda"))
load(file.path(baseFolder,"questTableMal.rda"))
load(file.path(baseFolder,"leverTableRav.rda"))
load(file.path(baseFolder,"questTableRav.rda"))
keptCols <- intersect(names(leverTableMal),names(leverTableRav))
leverTable <- rbind(leverTableMal[,keptCols],leverTableRav[,keptCols])
questTable <- rbind(questTableMal,questTableRav)

# levers names in stats and exp/bib
source("generalData.R") # correspondancesStatsElicitLevers and organismList

#==================================================
# Prepare the elicitation table for Merge
#                   group biblio et experts
#==================================================

preCols <- expand.grid(c("bin","normBin"),1:9)
cols <- c(paste0(preCols$Var1,preCols$Var2),"binTot","Pos","Neg","total")
grilleDeRef <- grep("*Grille-de-reference.json", leverTable$fileName)
sansGrilleDeRef <- ! leverTable$fileName %in% grilleDeRef
leverTableNoRef <- leverTable[sansGrilleDeRef,]

# table taking expert and biblio together
generalDBelicitation <- aggregate(leverTableNoRef[,cols],
                                  by=list(numQuestion=leverTableNoRef$numQuestion,
                                          subject=leverTableNoRef$subject),
                                  sum)
leverName <- aggregate(leverTableNoRef[,c("levier","buffer")],
                                  by=list(numQuestion=leverTableNoRef$numQuestion,
                                          subject=leverTableNoRef$subject),
                                  function(x){x[1]})
generalDBelicitation <- cbind(leverName,generalDBelicitation[,-c(1,2)])

generalDBelicitation <- merge(generalDBelicitation,correspondancesStatsElicitLevers,by.x="levier", by.y="elicitation",all.x=TRUE)

noNA <- ! is.na(generalDBelicitation$leverStats)
generalDBelicitationFin <-  generalDBelicitation[noNA,]

generalDBelicitationFin$meanElicitation <- apply(generalDBelicitationFin[,paste0("bin",1:9)],1,function(vect){ mean(rep(ElScale,vect))})
generalDBelicitationFin$sdElicitation <- apply(generalDBelicitationFin[,paste0("bin",1:9)],1,function(vect){ sd(rep(ElScale,vect))})
generalDBelicitationFin <- AddBinQuantiles(generalDBelicitationFin,"quantEl",probs=c(0.025,0.25,0.5,0.75,0.975),valBin=ElScale)

generalDBelicitationFin$lever.buffer <- paste0(generalDBelicitationFin$levier, generalDBelicitationFin$buffer)
generalDBelicitationFin <- merge(generalDBelicitationFin,organismList[,c("subject","latin")],by="subject") # by RYOHEI
# preparation d'un tableau bib seule
isFileBib <- grepl("*Bibliographie.json|*Nicolas-Guerin.json", leverTableNoRef$fileName)
leverTableBib <- leverTableNoRef[which(isFileBib),]
biblioDB <- aggregate(leverTableBib[,cols],
                      by=list(numQuestion=leverTableBib$numQuestion,
                              subject=leverTableBib$subject),
                      sum)
leverBib <- aggregate(leverTableBib[,c("levier","buffer")],
                       by=list(numQuestion=leverTableBib$numQuestion,
                               subject=leverTableBib$subject),
                       function(x){x[1]})
generalDBBib <- cbind(leverBib, biblioDB[,-c(1,2)])
generalDBBib <- merge(generalDBBib, correspondancesStatsElicitLevers,by.x="levier", by.y="elicitation",all.x=TRUE)
generalDBBibFin <-  generalDBBib[!is.na(generalDBBib$leverStats),]
generalDBBibFin$meanElicitationBib <- apply(generalDBBibFin[,paste0("bin",1:9)],1,function(vect){ mean(rep(ElScale,vect))})
generalDBBibFin <- AddBinQuantiles(generalDBBibFin,"quantBib",probs=c(0.025,0.25,0.5,0.75,0.975),valBin=ElScale)
generalDBBibFin$lever.buffer <- paste0(generalDBBibFin$levier, generalDBBibFin$buffer)
generalDBBibFin <- merge(generalDBBibFin,organismList[,c("subject","latin")],by="subject") # by RYOHEI
# preparation d'un tableau experts seuls
leverTableExp <- leverTableNoRef[which(!isFileBib),]
expertDB <- aggregate(leverTableExp[,cols],
                      by=list(numQuestion=leverTableExp$numQuestion,
                              subject=leverTableExp$subject),
                      sum)

leverExp <- aggregate(leverTableExp[,c("levier","buffer")],
                       by=list(numQuestion=leverTableExp$numQuestion,
                               subject=leverTableExp$subject),
                       function(x){x[1]})
generalDBExp <- cbind(leverExp, expertDB[,-c(1,2)])
generalDBExp <- merge(generalDBExp, correspondancesStatsElicitLevers,by.x="levier", by.y="elicitation",all.x=TRUE)
generalDBExpFin <-  generalDBExp[!is.na(generalDBExp$leverStats),]
generalDBExpFin$meanElicitationExp <- apply(generalDBExpFin[,paste0("bin",1:9)],1,function(vect){ mean(rep(ElScale,vect))})
generalDBExpFin <- AddBinQuantiles(generalDBExpFin,"quantExp",probs=c(0.025,0.25,0.5,0.75,0.975),valBin=ElScale)
generalDBExpFin$lever.buffer <- paste0(generalDBExpFin$levier, generalDBExpFin$buffer)
generalDBExpFin <- merge(generalDBExpFin,organismList[,c("subject","latin")],by="subject") # by RYOHEI

save(generalDBExpFin,file="generalDBExpFin.rda")
save(generalDBBibFin,file="generalDBBibFin.rda")
save(generalDBelicitationFin,file="generalDBelicitationFin.rda")


# # visualisation of the elicitation results for landscapes
# db <- generalDBExpFin
# maximizedCol <- "meanElicitationExp"
# baseNameQuant <- "quantExp"
# 
# source("statsFns.R")
generalDBelicitationMonoScale <- GetMaxLevOrg(generalDBelicitationFin,"meanElicitation")
# BoxPlotBy(generalDBelicitationMonoScale,"quantEl",ElRange=range(ElScale))
# dev.print(pdf,file="BxpsAllElicitation.pdf")
# 
generalDBExpMonoScale <- GetMaxLevOrg(generalDBExpFin,"meanElicitationExp")
# BoxPlotBy(generalDBExpMonoScale,"quantExp",ElRange=range(ElScale))
# dev.print(pdf,file="BxpsExpertsElicitation.pdf")
# 
# generalDBBibMonoScale <- GetMaxLevOrg(generalDBBibFin,"meanElicitationBib")
# BoxPlotBy(generalDBBibMonoScale,"quantBib",ElRange=range(ElScale))
# dev.print(pdf,file="BxpsBibliographieElicitation.pdf")

# #-----------------------------
# # elicitation experts + stats
# load("univarieStatsMonoScale.rda")
# univarieStatsMonoScale <- merge(univarieStatsMonoScale,organismList[c("subject","latin")])
# mergedMonoScale <- merge(generalDBExpMonoScale[,c("subject","levier","buffer","meanElicitationExp",
#                                                   "quantExp0.025","quantExp0.25","quantExp0.5",
#                                                   "quantExp0.75","quantExp0.975","binTot")], 
#                          univarieStatsMonoScale, 
#                          by.y=c("subject","elicitation"),by.x=c("subject","levier"),all.y=TRUE) 
# 
# xs <- BoxPlotBy(mergedMonoScale,"quantExp",ElRange=range(ElScale))
# summary(glm(mergedMonoScale$meanElicitationExp~mergedMonoScale$correlation))
# #=> correlation pas significative
# wilcox.test(mergedMonoScale$meanElicitationExp,mergedMonoScale$correlation)
# #=> un chouilla significatif
# dev.print(pdf,file="quantExpStats.pdf")
# 
# #-----------------------------
# # elicitation + stats
# mergedMonoScale <- merge(generalDBelicitationMonoScale[,c("subject","levier","buffer","meanElicitation",
#                                                           "quantEl0.025","quantEl0.25","quantEl0.5",
#                                                           "quantEl0.75","quantEl0.975","binTot")], 
#                          univarieStatsMonoScale, 
#                          by.y=c("subject","elicitation"),by.x=c("subject","levier"),all.y=TRUE) 
# 
# xs <- BoxPlotBy(mergedMonoScale,"quantEl",ElRange=range(ElScale))
# summary(glm(mergedMonoScale$meanElicitation~mergedMonoScale$correlation))
# #=> pas significatif
# wilcox.test(mergedMonoScale$meanElicitation,mergedMonoScale$correlation)
# #=> non significatif
# dev.print(pdf,file="quantEliStats.pdf")
# 
# #-----------------------------
# # elicitation biblio + stats
# mergedMonoScale <- merge(generalDBBibMonoScale[,c("subject","levier","buffer","meanElicitationBib",
#                                                   "quantBib0.025","quantBib0.25","quantBib0.5",
#                                                   "quantBib0.75","quantBib0.975","binTot")], 
#                          univarieStatsMonoScale, 
#                          by.y=c("subject","elicitation"),by.x=c("subject","levier"),all.y=TRUE) 
# 
# xs <- BoxPlotBy(mergedMonoScale,"quantBib",ElRange=range(ElScale))
# summary(glm(mergedMonoScale$meanElicitationBib~mergedMonoScale$correlation))
# #=> correlation pas significative
# wilcox.test(mergedMonoScale$meanElicitationBib,mergedMonoScale$correlation)
# #=> pas significatif
# dev.print(pdf,file="quantBibStats.pdf")
# 
# #-----------------------------

#### "modelName" focus tous bio-agresseurs
load("multiDBStatsFin.rda")
multiDBStatsFin <- as.data.frame(multiDBStatsFin)
multiDBStatsFin <- merge(multiDBStatsFin,organismList[,c("subject","latin")]) # To input latin name
# multiDBStatsFin <- merge(multiDBStatsFin,correspondancesStatsElicitLevers,by="leverStats") # To add elicitation
iLasso <- grep(modelName,multiDBStatsFin$idModel)
LassoStatsMonoscale <- GetMaxLevOrg(multiDBStatsFin[iLasso,],"partialCorrelation",colLevier="leverStats",colSubject="subject",colKeepOne="echelle")
#MalRavByCult <- rev(c("Altise","Meligethe","CharanconBT","CharanconTige","CharanconSil","PuceronEpis","PuceronAutomne","Cecidomyie","Pyrale",
#                      "Phoma","Sclerotinia","Septoriose","Rouille-Jaune","Rouille-Brune","PietinEchaudage","Mildiou"))
MalRavByCult <- rev(c("Psylliodes chysocephala","Meligethes aeneus","Ceutorhynchus picitarsis","Ceuthorhynchus napi","Ceutorhynchus assimilis",
                      "Aphidoidea","Rhopalosiphum padi","Cecidomyiidae","Ostrinia nubilalis","Leptosphaeria maculans","Sclerotinia sclerotiorum",
                      "Septoria tritici","Puccinia striiformis","Puccinia triticina","Gaeumannomyces graminis","Phytophthora infestans"))
# ## Experts
# LassoStatsMonoscale$estimate <- LassoStatsMonoscale$estimate *2
# mergedMonoScale <- merge(generalDBExpMonoScale[,c("subject","levier","buffer","meanElicitationExp",
#                                                   "quantExp0.025","quantExp0.25","quantExp0.5",
#                                                   "quantExp0.75","quantExp0.975","binTot")], 
#                          LassoStatsMonoscale, 
#                          by.y=c("subject","elicitation"),by.x=c("subject","levier"),all.y=TRUE) 
# 
# xs <- BoxPlotBy(mergedMonoScale,"quantExp",ElRange=range(ElScale),bioAgList=MalRavByCult,corCol="partialCorrelation",estCol="none")
# summary(glm(mergedMonoScale$meanElicitationExp~mergedMonoScale$partialCorrelation))
# # dev.print(pdf,file="quantExpLasso.pdf")
# wilcox.test(mergedMonoScale$meanElicitationExp,mergedMonoScale$partialCorrelation)
# # => something ?
# # ## biblio
# # mergedMonoScale <- merge(generalDBBibMonoScale[,c("subject","levier","buffer","meanElicitationBib",
# #                                                   "quantBib0.025","quantBib0.25","quantBib0.5",
# #                                                   "quantBib0.75","quantBib0.975","binTot")], 
# #                          LassoStatsMonoscale, 
# #                          by.y=c("subject","elicitation"),by.x=c("subject","levier"),all.y=TRUE) 
# # 
# # xs <- BoxPlotBy(mergedMonoScale,"quantBib",ElRange=range(ElScale),bioAgList=MalRavByCult,corCol="partialCorrelation",estCol="none")
# # summary(glm(mergedMonoScale$meanElicitationBib~mergedMonoScale$partialCorrelation))
# # wilcox.test(mergedMonoScale$meanElicitationBib,mergedMonoScale$partialCorrelation)
# # dev.print(pdf,file="quantBibLasso.pdf")

## biblio + experts
mergedMonoScale <- merge(generalDBelicitationMonoScale[,c("subject","levier","buffer","meanElicitation",
                                                  "quantEl0.025","quantEl0.25","quantEl0.5",
                                                  "quantEl0.75","quantEl0.975","binTot")], 
                         LassoStatsMonoscale, 
                         by.y=c("subject","elicitation"),by.x=c("subject","levier"),all.y=TRUE) 
bioAgList <- unique(mergedMonoScale$subject)
iOLinbioAgList <- match(organismList$subject,bioAgList)
bioAgList <- bioAgList[iOLinbioAgList[order(organismList$grpeCult,organismList$type,organismList$subject,decreasing=TRUE)]]

# vernacular french names
mergedMonoScale$partialCorrelationPvalSignif <- mergedMonoScale$partialCorrelation
mergedMonoScale$partialCorrelationPvalSignif[which(mergedMonoScale[,"p.value"]>0.05)] <- 0
xs <- BoxPlotBy(mergedMonoScale,"quantEl",ElRange=range(ElScale),namesCols=c("subject","buffer"),
                corCol="partialCorrelationPval",estCol="none",bioAgList=bioAgList)

dev.new()
xs <- BoxPlotBy(mergedMonoScale,"quantEl",ElRange=range(ElScale),namesCols=c("subject","buffer"),
                estCol="estimate",
                corCol="partialCorrelation",bioAgList=bioAgList)

# # latin names
# xs <- BoxPlotBy(mergedMonoScale,"quantEl",ElRange=range(ElScale),namesCols=c("latin","buffer"),
#                 corCol="partialCorrelation",
#                 estCol="none",bioAgList=unique(mergedMonoScale$subject))
# 
# 
# summary(glm(mergedMonoScale$meanElicitation~mergedMonoScale$partialCorrelation))
# wilcox.test(mergedMonoScale$meanElicitation,mergedMonoScale$partialCorrelation)
# dev.print(pdf,file="quantElLasso.pdf")
# 

# #### focus sur les ravageurs pour la plaquette stage Nicolas
# ## experts
# #ravNames <- sort(organismList$subject[which(organismList$type=="rav")],decreasing=TRUE) # default
# ravNames <- sort(organismList$latin[which(organismList$type=="rav")],decreasing=TRUE)
# iLassoRav <- which(grepl("Lasso",multiDBStatsFin$idModel) & multiDBStatsFin$latin %in% ravNames)
# 
# LassoStatsMonoscale <- GetMaxLevOrg(multiDBStatsFin[iLassoRav,],"partialCorrelation",colLevier="leverStats",colSubject="subject",colKeepOne="echelle")
# mergedMonoScale <- merge(generalDBExpMonoScale[,c("subject","levier","buffer","meanElicitationExp",
#                                                   "quantExp0.025","quantExp0.25","quantExp0.5",
#                                                   "quantExp0.75","quantExp0.975","binTot")], 
#                          LassoStatsMonoscale, 
#                          by.y=c("subject","leverStats"),by.x=c("subject","levier"),all.y=TRUE) 
# 
# xs <- BoxPlotBy(mergedMonoScale,"quantExp",ElRange=range(ElScale),bioAgList=ravNames,corCol="partialCorrelation",estCol="none")
# summary(glm(mergedMonoScale$meanElicitationExp~mergedMonoScale$partialCorrelation))
# wilcox.test(mergedMonoScale$meanElicitationExp,mergedMonoScale$partialCorrelation)
# dev.print(pdf,file="quantExpLassoRavageurs.pdf")
# 
# ## biblio
# mergedMonoScale <- merge(generalDBBibMonoScale[,c("subject","levier","buffer","meanElicitationBib",
#                                                   "quantBib0.025","quantBib0.25","quantBib0.5",
#                                                   "quantBib0.75","quantBib0.975","binTot")], 
#                          LassoStatsMonoscale, 
#                          by.y=c("subject","elicitation"),by.x=c("subject","levier"),all.y=TRUE) 
# 
# xs <- BoxPlotBy(mergedMonoScale,"quantBib",ElRange=range(ElScale),bioAgList=ravNames,corCol="partialCorrelation",estCol="none")
# summary(glm(mergedMonoScale$meanElicitationBib~mergedMonoScale$partialCorrelation))
# wilcox.test(mergedMonoScale$meanElicitationBib,mergedMonoScale$partialCorrelation)
# dev.print(pdf,file="quantBibLassoRavageurs.pdf")
# 
# ## biblio + experts
# mergedMonoScale <- merge(generalDBelicitationMonoScale[,c("subject","levier","buffer","meanElicitation",
#                                                   "quantEl0.025","quantEl0.25","quantEl0.5",
#                                                   "quantEl0.75","quantEl0.975","binTot")], 
#                          LassoStatsMonoscale, 
#                          by.y=c("subject","elicitation"),by.x=c("subject","levier"),all.y=TRUE) 
# 
# xs <- BoxPlotBy(mergedMonoScale,"quantEl",ElRange=range(ElScale),bioAgList=ravNames,corCol="partialCorrelation",estCol="none")
# summary(glm(mergedMonoScale$meanElicitation~mergedMonoScale$partialCorrelation))
# wilcox.test(mergedMonoScale$meanElicitation,mergedMonoScale$partialCorrelation)
# dev.print(pdf,file="quantElLassoRavageurs.pdf")

##  make table of regional effects per bioagressor
# list  regions
if(FALSE){
iRegions <- grep("^NA[^0-9]+",multiDBStatsFin[,"lever.buffer"])
listRegions <- unique(multiDBStatsFin$lever.buffer[iRegions])
listBioAg <- unique(multiDBStatsFin$subject)

matRegBioAg <- mat.or.vec(length(listRegions),length(listBioAg))
rownames(matRegBioAg) <- listRegions
colnames(matRegBioAg) <- listBioAg
for(reg in listRegions){
    for(bioAg in listBioAg){
        iKeep <- which(grepl("LassoNormMin",multiDBStatsFin$idModel)&
                       grepl(bioAg,multiDBStatsFin$subject)&
                       grepl(paste0("^",reg,"$"),multiDBStatsFin$lever.buffer))
        if(length(iKeep)==0){
            out <- 0
        }else if(length(iKeep)==1){
            out <- multiDBStatsFin[iKeep,"partialCorrelation"]
        }else{
            browser()
        }
        matRegBioAg[reg,bioAg] <- out
    }
}
rownames(matRegBioAg) <- gsub("NA","",listRegions)
print(matRegBioAg)


}
