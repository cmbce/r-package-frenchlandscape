library(frenchLandscapeSynchronization)
setwd(GetDicoPath("r-package-frenchlandscape"))
source("statsFns.R")

########################
# Import and merge data
########################
# métriques bioagresseur
bioAg_metrics <- ExtLoad("bioagresseurs","altise/altise_metrics") # metricsMeligethe, obtenu grâce au script exampleStatBioAg.R (bioagresseur) 
metricsMeligethe <- bioAg_metrics[which(bioAg_metrics$Modif.Coord..GPS=="Oui"),]

# Choses à changer :
# Ctrl+F CeciOrangeCuvette_CountAbove_10 (x2)
# Ctrl+F pyrale (x2)
# Si un seul seuil ok: y2, sinon ajouter les y5 (x3-4)
gCulture <- "g5" #1:blé ; 2:maïs ; 5:colza

# métriques paysagères
LM <- ReadMetrics()
metricsMeligethe <- ForceProjOfFirst(frenchDepartements,metricsMeligethe)
LMclean <- LM[,-grep("RPG.*g1e",names(LM))]

metricsMeligethe$coordsAsString <- GetCoordsAsString(metricsMeligethe)
metricsMeligethe <- spatDataManagement::MergeToSpObject(spObj=metricsMeligethe,df=LM,by="coordsAsString",all.sp=TRUE)

########################
# métriques composites 
########################
metricsMeligethe <- MakeCompositeMetrics(metricsMeligethe,gCulture)

iRPGSame <- grep("RPGSame_w.*_area",colnames(metricsMeligethe@data))
iRPGPrec <- grep("RPGPrec_w.*_area",colnames(metricsMeligethe@data))
iBDTOPO2012noVeg <- grep("BDTOPO2012[^v].*_w.*_area",colnames(metricsMeligethe@data))
iPrairiesSame <- grep("PrairiesSame.*_w.*_area",colnames(metricsMeligethe@data))
iColReg <- c(iRPGSame,iRPGPrec,iBDTOPO2012noVeg,iPrairiesSame)

### identifie rangs avec des NA
rowReg <- which(apply(metricsMeligethe@data[,iColReg],1,function(vect){all(!is.na(vect))}))
notOneNA <- apply(metricsMeligethe@data[,iColReg],1,function(vect){all(!is.na(vect))})

### identifie rangs de régions ayant trop peu de points
# matFactors <- model.matrix(metricsMeligethe@data$MelNbP_CountAbove_2[rowReg]~metricsMeligethe@data[rowReg,"grande.région"]+Campagne)[,-1]
# aggregate(matFactors,by=list(matFactors[,"Campagne"]),sum)
#=> eliminate bretagne, est humide, poitou - façade atlantique, sud-bassin parisien
regionTooFew <- c("bretagne", "est humide", "poitou - façade atlantique", "sud-bassin parisien")
colnames(metricsMeligethe@data)[grep("grande",names(metricsMeligethe@data))] <- "grandeRegion"
notTooFew <- ! metricsMeligethe@data[,"grandeRegion"] %in% regionTooFew
# NB: la région Bassin parisien devient la région de référence

### retire indésirables
rowReg <- which(notOneNA & notTooFew)

### if want to control for years, need 
# Campagne <- metricsMeligethe@data[rowReg,"Campagne"]
# matFactors <- model.matrix(metricsMeligethe@data$MelNbP_CountAbove_2[rowReg]~metricsMeligethe@data[rowReg,"grande.région"]+Campagne)[,-1]
# for some reason lassoscore really doesn't like it...
### else
# rowReg <- which(notOneNA)
matFactors <- model.matrix(metricsMeligethe@data$GApercentM_CountAbove_10[rowReg]~factor(metricsMeligethe@data[rowReg,"grandeRegion"]))[,-1]
### fi

colnames(matFactors) <- gsub("^.*]","",colnames(matFactors))

y1 <- metricsMeligethe@data$GApercentM_CountAbove_10[rowReg]
X <- as.matrix(data.frame(metricsMeligethe@data[rowReg,iColReg],matFactors))

# and for glm
Xglm <- model.matrix(y1~X)


####################
# Model(s)
####################
#===========
# simple multi-variate glm
#===========
print(summary(glm(y1~Xglm,family="poisson")))
#=> can see the "prairies" but not much more

#===========
# Lasso
#===========
# res2 <- LassoPval(y2,X) # par défault maintenant utilise lambda.1se, lambda plus économe en paramètres
# plot(res2@cv) # a good idea to understand how lambda can change quickly. exp(-3.7) est probablement mieux ici
#               # je pense qu'au final il faudra prendre à la main le meilleur lambda pour chaque seuil de chaque
#               # bioagresseur
# res2 <- LassoPval(y2,X,lambda=exp(-3.7))
# res2.min <- LassoPval(y2,X,lambda="lambda.min") # plus de paramètre (erreur de prédiction minimale)
# res5 <- LassoPval(y5,X) 

# probably the best default: find the elbow between min (usually flat) and 1se (possibly quite far)
#    find where the curve "picks-up" and uses this as the lambda
#res1.elbow <- LassoPval(y1,X,lambda="polynomial.elbow")

# NB: les p-values peuvent changent un peu d'une exécution à une autre, c'est dû aux lambda.min identifiés par la validation croisée (processus aléatoire) qui ne sont pas toujours les mêmes peut-être qu'il faudrait faire une moyenne des différentes p-values qu'on obtient mais ça peut attendre. Pour l'instant géré en multipliant les folds

# NB2: j'ai ajouté le coef sur variable normalisée pour avoir une idée de la taille de l'effet par rapport à la variabilité dans le paysage (les p-values sont les mêmes)

# NB3: pour accéder à D2/D2 cor on peut utiliser res2@D2 par exemple

# NB4: pour l'instant il faut s'en tenir je pense aux résultats de  
# res2 <- LassoPval(y2,X)
# avec les coefs "normalisés" et les p-values sand.conf pour être sûr de pas risquer de dire trop de bêtises 


## une autre manière de visualiser: classe simplement les variables par ordre d'apparition (du départ au modèle
## avec erreur minimum) le chiffre correspond au nombre de fois où il est sélectionné (le plus haut est la 
## variable la plus sélectionné donc à priori la plus fiable
#print(LassoTreeSummary(res1.elbow))
#print(LassoTreeSummary(res2.elbow))

# Note: provence is pretty good at getting some but not at getting much...

mod <- glmnet(y=y1,x=X,family="poisson")
XNorm <- apply(X,2,AutoScale)
modNorm <- glmnet(y=y1,x=XNorm,family="poisson")

billboard <- LassoTreeSummaryBis(mod)
billboardNorm <- LassoTreeSummaryBis(modNorm)
#=> garder les 20 meilleurs lambda paraît sélectionner assez sévèrement les 15 meilleurs variables
meanCoefs <- apply(mod$beta[names(billboard),1:20],1,mean)
meanCoefsNorm <- apply(modNorm$beta[names(billboardNorm),1:20],1,mean)
tableBillCoefs <- data.frame(cbind(billboard,meanCoefs,meanCoefsNorm))
tableBillCoefs

metricName <- c("RPGSame", "RPGPrec", "BDTOPO2012bois", "BDTOPO2012haie", "BDTOPO2012lande", "PrairiesSame")
outFolderPdf <- paste0(GetDicoPath("bioagresseurs"),"/altise/pdf")
for(num in 1:length(metricName))
{
  if(!file.exists(outFolderPdf)){dir.create(outFolderPdf)}
  pdf(file = paste0(outFolderPdf,"/grapheLasso_",metricName[num],".pdf"))
  #plot(0.5*c(-1,1),c(0,1),type="n",yaxt="n",xlab="meanCoefsNorm",ylab="",main=metricName[num]) # Visu perso avec les titres, pas fait pour rentrer dans un tableau
  plot(0.1*c(-1,1),c(0,1),type="n",yaxt="n",ann=F,main=metricName[num])
  abline(v=0,col="gray")
  abline(v=tableBillCoefs[["meanCoefsNorm"]][grep(metricName[num],rownames(tableBillCoefs))[1]],col="blue") # 1 car on veut celui qui sort en premier
  dev.off()
}
