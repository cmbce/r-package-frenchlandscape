
########################
# 
# Script:  
#
# Purpose: 
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

### necessary packages (including embeded data)
library("frenchLandscape")
library("rgeos")

### Load data
# exemple 95
load(GetDicoPath("RPG"),"ilotsCult_2014_095.rda")
e(frenchDepartements[which(frenchDepartements$CODE_DEPT=="95"),])

iBois <- which(vegetation_2017_095$NATURE %in% c("bois","coniferes","feuillus","f.mixte","f.ouverte"))
areaBois <- sum(vegetation_2017_095$area[iBois])
iVegNotBois <- setdiff(1:dim(vegetation_2017_095)[1],iBois)
areaVegNotBois <- sum(vegetation_2017_095$area[iVegNotBois])


areaPerCult <- aggregate(ilotsCult_2014_095$SURFACE_GROUPE_CULTURE,by=list(gCult=ilotsCult_2014_095$CODE_GROUPE_CULTURE),sum)
areaPerCult$x <- areaPerCult$x*10000 # ha to m2
areaCult <- sum(areaPerCult$x)

areas <- data.frame(areaDep=areaDep,rateCult=areaCult/areaDep,rateBois=areaBois/areaDep,
                    rateVegNotBois=areaVegNotBois/areaDep)

areaPerCult<-areaPerCult[order(areaPerCult[,"x"]),]
areaPerCult$pcSole <- round(areaPerCult$x/areaCult *100,1)

areaPerCult$cult <- RPG::RPGcodes()[match(areaPerCult$gCult,RPG::RPGcodes()[,2]),1]

