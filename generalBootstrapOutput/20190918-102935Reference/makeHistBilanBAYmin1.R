graphics.off()
source("generalDataBAYmin1.R")
source("statsFns.R")
# plots the sign function of leverStats
# plots the cumulated coefs (and scale weighted by the coef)
CoefsBarplot <- function(db,groups="leverStats",response="partialCorrelation",
                        col=c("green","white","red"),space=c(0,2),
                        xlab="Element paysager",ylab="Nombre de bioagresseurs",
                        ...){
    # ici faire une somme des coefs au lieu d'un compte
    counts <- table(db[,c(groups)],sign(db[,response]))

    row.names(counts) <- jolisNomsVariables[match(row.names(counts),jolisNomsVariables$leverStats),"french"]
   
    xs <- barplot(t(counts),beside=TRUE,col=col,
                  space=space,xlab=xlab,ylab=ylab,
                  ,ylim=c(0,max(counts)+2),...)
    ### add distance information
    # get the average distance that matter
    db$signPC <- sign(db[,response])

    meanScale <- aggregate(db[,"echelle",drop=FALSE],by=list(leverStats=db$leverStats,signPC=db$signPC),
                   function(x){mean(as.numeric(as.character(x)))})
    meanScale$leverStats <- as.character(meanScale$leverStats)
    # when makes sens (different from leverStats) show it

    xs <- xs
    ys <- t(counts)+0.2
    # points(xs,ys)
    countsSimpleNames <- jolisNomsVariables[match(row.names(counts),jolisNomsVariables$french),"leverStats"]
    scales <- NA*xs
    for(iDim2 in 1:length(countsSimpleNames)){
        for(iDim1 in c(1,3)){ # don't want scales for no effect
            iScale <- which(meanScale$leverStats==countsSimpleNames[iDim2] & 
                            meanScale$signPC==c(-1,0,1)[iDim1])
            if(length(iScale)==1){
                scales[iDim1,iDim2] <- meanScale[iScale,"echelle"]
            }
        }
    }
    text(xs,ys,labels=round(scales,-1),srt=90,pos=4,offset=0.1)
    attr(counts,"xs") <- xs
    attr(counts,"ys") <- ys
    attr(counts,"scales") <- scales

    return(invisible(counts))
}
# CoefsBarplot(simpleDB)

## load Data
load("multiDBStatsFin.rda")
multiDBStatsFin <- as.data.frame(multiDBStatsFin,stringsAsFactors=FALSE)
multiDBStatsFin$type <- organismList$type[match(multiDBStatsFin$subject,organismList$subject)]

## select data
iLasso <- grep(modelName,multiDBStatsFin$idModel)
load("generalDBBibFin.rda") # necessairepour GetMaxLevOrg
multiDBStatsFin$CompPValue <- 1-multiDBStatsFin$p.value

if(grepl("^(glm|binomial)",modelName)){
    maximizedCol <- "CompPValue"
}else if(grepl("LassoLogNorm",modelName)){
    maximizedCol <- "partialCorrelation"
}else{
    stop("Maximized col not defined for this modelName")
}
grepGetVal <- function(pattern,x,...){
    return(x[grep(pattern,x,...)])
}
proxNames <- grepGetVal("prox",unique(multiDBStatsFin$leverStats))
LassoStatsMonoscale <- GetMaxLevOrg(multiDBStatsFin[iLasso,],maximizedCol=maximizedCol,
                                    colLevier="leverStats",colSubject="subject",colKeepOne="echelle",
                                    levs=c("bois","GetLandeIGN","haie","PrairiesSame","Prec","Same",
                                           "GetBoisTraitesIGN","GetVergerIGN", "GetPeupleraieIGN", "GetVigneIGN",proxNames,"MedMoy_Dept_Same", "MedMoy_Dept_Ymin1", "MedMoy_cor_Dept_Same", "MedMoy_cor_Dept_Ymin1","Prec_Dept_Ymin1", "Prec_cor_Dept_Ymin1","ETP", "ETP_Year", "Precip", "Precip_per_", "Precip_per__Year", "Precip_Year", "Ray", "Ray_Year", "Tmax", "Tmax_bet010", "Tmax_bet010_Year", "Tmax_ab34","Tmax_ab34_Year", "Tmax_bel17", "Tmax_bel17_Year", "Tmax_Year", 
"Tmin", "Tmin_Year"))

# besoin d'ajouter référence ratioCult
simpleDBColumns <- c("subject","leverStats","echelle","lever.buffer","estimate","p.value","partialCorrelation","type")

simpleDB <- multiDBStatsFin[iLasso,simpleDBColumns]

head(LassoStatsMonoscale[,simpleDBColumns])


# note that estimate and partialCorrelation are of same sign and largely proportional
dev.new()
plot(simpleDB$estimate,simpleDB$partialCorrelation,main="toutes variables")
points(LassoStatsMonoscale$estimate,LassoStatsMonoscale$partialCorrelation,col="blue",pch=3)
sel<- which(simpleDB$leverStats == "ratioCult")
points(simpleDB$estimate[sel],simpleDB$partialCorrelation[sel],col="green",pch=3)
sel <- which(simpleDB$leverStats %in% 2008:2014)
points(simpleDB$estimate[sel],simpleDB$partialCorrelation[sel],col="red",pch=3)
sel <- which(gsub("\\."," ",simpleDB$leverStats)%in% levels(grandesRegions[,"grande.région"]))
points(simpleDB$estimate[sel],simpleDB$partialCorrelation[sel],col="red",pch=8)
abline(h=0)
abline(v=0)
#=> que sont les points qui n'ont toujours pas de croix?

# brutal sur toutes les échelles etc.
if(FALSE){
SignBarplot(simpleDB)

# seulement prox
iProx <- grep("prox",simpleDB$leverStat)
if(length(iProx)>0){
    dev.new()
    SignBarplot(simpleDB[iProx,])
    print(simpleDB[which(grepl("prox",simpleDB$leverStat)&simpleDB$estimate!=0),])
}
}

# un peu moins brutal, pas toutes les échelles
MonoScale <- rbind(LassoStatsMonoscale[,simpleDBColumns],simpleDB[which(simpleDB$leverStats %in% "ratioCult"),])
save(MonoScale, file="MonoScale.rda") 
# we hide the control variables from MonoScale 
MonoScale <- MonoScale[MonoScale$leverStats %nin% c("GetBoisTraitesIGN","GetVergerIGN", "GetPeupleraieIGN", "GetVigneIGN"),]

## make histogram of effects sign on the Monoscale

dev.new()
# standard raisonable
par(mfrow=c(3,1))
pValThr <- 0.05
thrResponse <- 0
SignBarplot(MonoScale[MonoScale$type=="rav",],main="Pests",pValThr=pValThr,thrResponse=thrResponse)
SignBarplot(MonoScale[MonoScale$type=="mal",],main="Pathogens",pValThr=pValThr,thrResponse=thrResponse)
SignBarplot(MonoScale,main="All bioagressors",pValThr=pValThr,thrResponse=thrResponse)
# dev.print(dev=pdf,file="BarplotSignNoCultRot.pdf")
# SignBarplot(MonoScale[MonoScale$type=="rav",],main="Ravageurs")
# dev.print(dev=png,file="BarplotSignRavMalBase.png", width = 650, height = 750, res = 110)

# visualiser tout
dev.new()
par(mfrow=c(3,2))
SignBarplot(MonoScale,main="Tous, toutes valeurs (pval 1,thr 0)",pValThr=1,thrResponse=0)
SignBarplot(MonoScale,main="Tous, restrictif val (pval 1, thr 0.05)",pValThr=1,thrResponse=0.05)
SignBarplot(MonoScale,main="Tous, base (pval 0.05, thr0)",pValThr=0.05,thrResponse=0)
SignBarplot(MonoScale,main="Tous, base stat, restrictif val (pval 0.05, thr 0.05)",pValThr=0.05,thrResponse=0.05)
SignBarplot(MonoScale,main="Tous, restrictif stat (pval=0.05/20, thr 0)",pValThr=0.05/20,thrResponse=0)
SignBarplot(MonoScale,main="Tous, restrictif pval et stat (pval=0.05/20, thr 0.05)",pValThr=0.05/20,thrResponse=0.05)
#

# ajouter quelque chose qui donne une idée des tailles d'effets par rapport aux années/régions

# il faudrait avoir 4 niveau pour le temporel (ratioCult) comme pour l'espace : 
# - année précédente (toujours dispo)
# - 2 années précédentes (toujours dispo)
# - 5 années centrées sur y ou y+4 précédentes pour observations 2013 et 2014
# - 8 années 

# il faudrait aussi ajouter l'historique de prairie sur l'ilot : pour les cécidomyies ça peut casser le cycle?
# et retirer l'effet spatial ?
multiDBStatsFin[which(grepl("Lasso",multiDBStatsFin$idModel) & multiDBStatsFin$subject=="Meligethe" &
                multiDBStatsFin$leverStats %in% c("bois","haie","PrairiesSame","Prec","Same") &
                multiDBStatsFin$estimate != 0),
            c("leverStats","echelle","estimate","partialCorrelation")]


# autres paramètres

iNoScale <- which(!grepl("^(200|1000|5000|10000)$",multiDBStatsFin$echelle))
VarNoScale <- multiDBStatsFin[intersect(iNoScale,iLasso),]
dev.new(width=16,height=10)
# par(mar=c(5,10,5,3))
SignBarplot(VarNoScale,main="NoScale parameters",pValThr=pValThr,thrResponse=thrResponse,
            las=1,horiz=FALSE,
            xlab="Nombre de bioagresseurs",ylab="Oiseaux")

# visualize all together for publication
dev.new(width= 10.97916,height=4.810258)
iToKeep <- which(!MonoScale$leverStats %in% c("GetBoisTraitesIGN","GetLandeIGN") &
                 !MonoScale$subject %in% c("Oiseaux"))
                   
toPlot <- MonoScale[iToKeep,]

SignBarplot(toPlot,main="All bioagressors",pValThr=pValThr,thrResponse=thrResponse,keepNA=FALSE)
# dev.print(pdf,file="imageChapter.pdf")

