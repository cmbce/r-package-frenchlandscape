# correspondance entre nom de statistiques paysagères et noms de leviers paysagers dans le questionnaire
options(stringsAsFactors=FALSE)
correspondancesStatsElicitLevers <- data.frame(matrix(c("bois","bois",
                                             "vegetation",NA,
                                             "boisTraites",NA,
                                             "haie","haies",
                                             "lande",NA,
                                             "PrairiesSame","prairies",
                                             "Same","Same",
                                             "Prec","Prec",
                                             NA, "jardins",
                                             NA, "Enfouissement",
                                             NA, "herbe",
                                             NA, "TailleParc",
                                             "MedMoy_Dept_Same", NA, 
                                             "MedMoy_cor_Dept_Same", NA, 
                                             "MedMoy_cor_Dept_Ymin1", NA, 
                                             "MedMoy_Dept_Ymin1", NA, 
                                             "Prec_Dept_Ymin1", NA, 
                                             "Prec_cor_Dept_Ymin1", NA,

                             "Precip",NA,
                             "Precip_Year",NA ,
                             "ETP", NA,
                             "ETP_Year", NA , 
                             "Tmax",NA,
                             "Tmax_Year",NA, 
                             "Tmin", NA, 
                             "Tmin_Year",NA, 
                             "Ray", NA,
                             "Ray_Year", NA, 
                             "Precip_per_",NA, 
                             "Precip_per__Year",NA, 
                             "Tmax_ab34",NA, 
                             "Tmax_ab34_Year",NA,
                             "Tmax_bel17", NA, 
                             "Tmax_bel17_Year", NA,
                             "Tmax_bet010",NA, 
                             "Tmax_bet010_Year",NA

                                             ),ncol=2,byrow=TRUE,dimnames=list(NULL,c("leverStats","elicitation"))))

CNOL <- c("organism","elObs","elObsSN",    "trLow",              "trHigh",          "grpeCult","subject","type","latin","english","culture", "saison_organisme", "saison_culture")
# have one line per bio-agressor, grpCult as in RPG::RPGcodes(), 25 for the "pomme de terre" but check (see R/frenchlandscape/coherence_vigiculturesRpg)

# to see what to put, see studyMetrics.R

IVOL <- c(
          "altise_g","GANbPE","GANbPE", 1,5,"g5","Altise","rav","Psylliodes chysocephala", "Rape flea beetle","colza",                              "automne", "hiver",
          "altise_p","PANbV","PANbV",1,5,"g5","Petite altise","rav","Phyllotreta nemorum","Turnip flea beetle","colza",                             "automne", "hiver",
          "cecidomyie","CECI_ORANGE_CUV_JAU","PresenceAdulte",1,2,"g1","Cecidomyie","rav","Sitodiplosis mosellan","Gall midges","bleTH",                    "printemps", "hiver",
          "charanconBT", "CBTNbV","CBTNbV",2,5,"g5","CharanconBT","rav","Ceutorhynchus picitarsis","rape weevil?","colza",                          "automne", "hiver",
          "charanconSil","ChSNbP","ChSNbP",1,3,"g5","CharanconSil","rav","Ceutorhynchus assimilis","cabbage seed weevil","colza",                   "printemps", "hiver",
          "charanconTige","ChTNbV","ChTNbV",2,5,"g5","CharanconTige","rav","Ceuthorhynchus napi","rape steem weevil","colza",                       "printemps", "hiver",
          "fusariose","FUSA_TIG_%","Fusariose_base_tige_%",1,4,"g1","Fusariose","mal","Fusarium Graminearum","Fusarium wilt","bleTH",      "printemps", "hiver",
          "helminthosporioseBTH","HELMIN_F3","HELMIN_F3",1,5,"g1","Helminthosporiose BTH","mal","Helminthosporium (wheat)","Silver scurf","bleTH",          "printemps", "hiver",
          "helminthosporioseOrgeH","HELMIN_F3","HELMIN_F3",1,5,"g3","Helminthosporiose OH","mal","Helminthosporium (oat) ","Silver scurf","orgeH",         "printemps", "hiver",
          "limaces","D_PL_LIMA_%","PerDegLim",1,5,"g1","Limaces","rav","Deroceras|Arion|Limax","slug","bleTH",                                     "automne", "hiver",
          "meligethe", "Mel%P","MelPerP",2,5, "g5","Meligethe","rav","Meligethes aeneus","Pollen beetle","colza",                                   "printemps", "hiver",
          "mildiou","MILDIOU","Mildiou",1,3,"g25","Mildiou","mal","Phytophthora infestans","Potato late blight","pdt",                              "printemps", "printemps",
          "oidiumBTH","OIDF3","OIDF3",1,3,"g1","OïdiumF3","mal","Blumeria graminis","barley powdery mildew","bleTH",                                   "printemps", "hiver",
          #"oiseau","OISEAUX","OISEAUX",1,3,"g1","Oiseaux","rav","Ave spp.","birds","bleTH",
          "phoma","Pho%N","PhoPercentN",1,10,"g5","Phoma","mal","Leptosphaeria maculans","Blackleg disease","colza",                                "printemps", "hiver",
          "pietinEchaudage","PIE_ECH_%","bilanPE",1,5,"g1","PietinEchaudage","mal","Gaeumannomyces graminis","Take-all","bleTH",                    "printemps", "hiver",
          "pietinVerse","PV_%","bilanPV",1,5,"g1","PietinVerse","mal","Oculimacula spp.","Eyespot","bleTH",                                         "printemps", "hiver",
          "puceronAutomne","PUC_AUT_PLANT_%","puceronAutomnePourcent",1,5,"g1","PuceronAutomne","rav","Rhopalosiphum padi","Bird cherry-oat aphid","bleTH", "automne", "hiver",  # note: could be best to account for PUC_AUT_COND_OBSV
          "puceronCendre","PCNbC","PCNbC",1,5,"g5","PuceronCendre","rav","Brevicoryne brassicae","cabbage aphid","colza",                           "printemps", "hiver",
          "puceronEpis","PUC_EPI_PLANT_%","PuceronEpisPourcent",1,5,"g1","PuceronEpis","rav","Sitobion avenae","Aphid","bleTH",                     "printemps", "hiver",
          "puceronVert","PV%P","PuceronVertPourcent",1,5,"g5","PuceronVert","rav","Myzus persicae","green peach aphid","colza",                     "automne", "hiver",
          "pyrale","PYR_PHE_NB_ADULTES","PyrPieges",1,4,"g2","Pyrale","rav","Ostrinia nubilalis","Corn borer","mais",                               "printemps", "printemps",
          "rhynchosporiose","RHYNCF3","RHYNCF3",1,4,"g3","Rhyncho","mal","Rhynchosporium secalis","barley scald","orgeH",                           "printemps", "hiver",
          "rouilleJaune","ROUIL_JAU_F3","rouilleJauneF3",1,4, "g1","Rouille-Jaune","mal","Puccinia striiformis","Yellow rust","bleTH",              "printemps", "hiver",
          "rouilleBrune","RBF3","rouilleBruneF3",1,4, "g1","Rouille-Brune","mal","Puccinia triticina","Brown rust","bleTH",                         "printemps", "hiver",
          "sclerotinia","K1%F+","sclerotiniaKPerF",50,80,"g5","Sclerotinia","mal","Sclerotinia sclerotiorum","White mold","colza",                  "printemps", "hiver",
          "septoriose","SEPF3","Septoriose F3",1,2,"g1","Septoriose","mal","Septoria tritici","Septoria leaf blotch","bleTH",                       "printemps", "hiver",
#New ITB 
          "ramulariose",  "RAMU__%F" ,"RAMU__%F","NA" ,"NA" ,"g24"  ,"Ramulariose" ,"mal" , "Ramularia betae" , "Ramularia leaf spot" ,"betterave" ,            "printemps", "printemps",
          "cercosporiose","CERCO_%F" ,"CERCO_%F","NA" ,"NA" ,"g24"  ,"Cercosporiose" ,"mal" , "Cercospora beticola" , "Cercospora leaf spot" ,"betterave" ,     "printemps", "printemps",
          "oidiumBET",    "OIDIUM_%F" ,"OIDIUM_%F","NA" ,"NA" ,"g24"  ,"OidiumBetterave" ,"mal" , "Erysiphe betae" , "Beet powdery mildew" ,"betterave" ,       "printemps", "printemps",
          "rouilleBET",   "ROUILLE_%F" ,"ROUILLE_%F","NA" ,"NA" ,"g24"  ,"RouilleBetterave" ,"mal" , "Uromyces betae" , "Beet rust" ,"betterave" ,              "printemps", "printemps"
          #"sesamie",  "",   "",   "g1","Sesamie","rav","","",
)
organismList <- data.frame(matrix(IVOL, ncol=length(CNOL), byrow=TRUE,dimnames=list(NULL, CNOL)),stringsAsFactors=FALSE)
rm(CNOL,IVOL)

jolisNomsVariables <- matrix(c("bois",      "Bois",         "Woods",
                               "GetLandeIGN", "Landes", "Scrubland",
                               "haie",      "Haies",        "Hedgerows",
                               "PrairiesSame","Prairies",   "Grasslands",
                               "Prec",      "Culture (N-1)","Host crop\n(y-1)",
                               "Same",      "Culture (N)",  "Host crop\n(y)",
                               "GetBoisTraitesIGN", "Bois traités", "Woody crops",
                               "GetVigneIGN", "Vignes", "Vineyard",
                               "GetVergerIGN", "Verger", "Orchyard",
                               "GetPeupleraieIGN", "Peupleraie", "Poplar trees" ,
                               "proxPrairie","Temps depuis\nprairie",  "Time since\ngrassland",
                               "proxRot",   "Temps depuis\nculture",  "time since\ncrop",
                               "proxPrairie3","Temps depuis\nprairie",  "Time since\ngrassland",
                               "proxRot3",   "Temps depuis\nculture",  "Time since\ncrop",
                               "ratioCult", "Cult. rotation",   "Crop in rotation",
#Added for N-1 bioaresseur
                               "MedMoy_Dept_Same", "Pression dept.", "Dept. Pressure", 
                               "MedMoy_Dept_Ymin1", "Pression dept. (N-1)","Dept. pressure (y-1)",
                               "MedMoy_cor_Dept_Same", "Cor. pression dept.", "Cor. dept pressure",
                               "MedMoy_cor_Dept_Ymin1", "Cor. pression dept. (N-1)", "Cor. dept. pressure y-1)",
                               "Prec_Dept_Ymin1", "Pression dept. x Culture (N-1)", "Dept. pressure x Crop (y-1)",
                               "Prec_cor_Dept_Ymin1","Cor. pression dept.\nx Culture (N-1)", "Cor. dept. pressure\nx Crop (y-1)", 
#Added for Meteo dept/months 
                             "Precip","Precipitations\n(mm/jour)", "Precipitations \n(mm/day)",
                             "Precip_Year","Precipitations annuelles\n(mm/jour)","Annual precipitations\n(mm/day)" ,
                             "ETP",  "ETP (mm/jour)",  "ETP (mm/day)",
                             "ETP_Year", "ETP annuelle\n(mm/jour)", "Annual ETP\n(mm/day)" , 
                             "Tmax","Temperature max.\n(°C)","Max. temperature\n(°C)",
                             "Tmax_Year","Temperature max.\nannuelle (°C)", "Annual max.\ntemperature (°C)", 
                             "Tmin", "Temperature min.\n(°C)","Min. temperature\n(°C)", 
                             "Tmin_Year","Temperature min.\nannuelle (°C)","Annual min.\ntemperature (°C)", 
                             "Ray",  "Rayonnement solaire","Solar radiation",
                             "Ray_Year", "Rayonnement\nsolaire annuel","Annual solar\nradiation", 
                             "Precip_per_","Inconnu", "Unknown", 
                             "Precip_per__Year","Inconnu", "Unknown", 
                             "Tmax_ab34","Nbr. jours\n> 34°C" , "No. days\n> 34°C", 
                             "Tmax_ab34_Year", "Nbr. jours dans l'année\n> 34°C", "No. days in the year\n> 34°C°C" ,
                             "Tmax_bel17", "Nbr. jours\n< 17°C" , "No. days\n< 17°C", 
                             "Tmax_bel17_Year", "Nbr. jours dans l'année\n< 17°C", "No. days in the year\n< 17°C" ,
                             "Tmax_bet010", "Nbr. jours\nentre 0 et 10°C" , "No. days\nbetween 0 and 10°C", 
                             "Tmax_bet010_Year", "Nbr. jours dans l'année\nentre 0 et 10°C", "No. days in the year\nbetween 0 and 10°C") ,
                               byrow=TRUE,ncol=3)
colnames(jolisNomsVariables) <- c("leverStats","french","english")
jolisNomsVariables <- data.frame(jolisNomsVariables)

