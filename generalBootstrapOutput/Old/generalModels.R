# Libraries et fonctions
#=================================
library("frenchLandscape")
library("MASS")
source("statsFns.R")

#=================================
# Parameters
#=================================
external_parameterization <- FALSE # to comment if using setparameters
#source("parameters_sampler.r") # to activate if using setparameters

if(external_parameterization == FALSE) {

    nameSimu <- "ManualBootstrap"
    #"VC3_cgr_GetIGN_proxRotPrairie3_LassoNormMinBin_nAboveMedMoy"
    #baseModel 2019: getIGN(all) proxRot(Prairie/Cult)3, LassoNormMinBin, nAboveMedMoy

    modelName <- "LassoNormMinBin" # LassoNormMin, LassoNormMinPA, LassoNormMinBin, LassoNormMinGaus, glmnb, glmBinomial, glmPA, glmer.nb, glmerBin

    metricModel <- "nAboveMedMoy"   # metricModelMedian, metricModelLow,metricModelHigh, nAboveMedMax, meanObs, maxObs, nAboveQlowMax, nAboveQhighMax, nAboveMedMoy
 
    metricsGroup <-   paste0("iRPGSame,iRPGPrec,iBDTOPOGetAllIGN,iPrairiesSame,iRPGproxRot3,iRPGproxPrairie3") 
    #   possibilities: iRPGSame,iRPGSameNo10000,iRPGPrec,iRPGPrecNo10000,
    #                  iBDTOPOGetBoisHaie,iBDTOPOGetAllIGN,iBDTOPOGetBoisHaieIGN,iBDTOPOGetBoiTraitesIGN,iBDTOPOGetPVVIGN,iBDTOPOGetLandeIGN
    #                  iPrairiesSame,iPrairiesSameNo10000,
    #                  iPC,iPC1_2
    #                  iRatioCult
    #                  iRPGproxRot,iRPGproxRot3
    #                  iRPGproxPrairie,iRPGproxPrairie3

} 
    # included in the regression as factors
    factorNames <- c("segArvSysteme","campagne_grandeRegion") # "grandeRegion","segArvSysteme","campagne"
    # should add "reseaux"

    # random effect (accounted for only if model is glmer*)
    glmerRE <- "+(1|campagne:grandeRegion)"

    # année bois (BDTOPO)
    yearVeg <- 2017 # 2012 or 2017
    # included in the regression as continuous metrics

  
    ## observations choice
    regionTooFew <- c("bretagne")

    ## could auto-select the meaningful regions by bio-agressor (>30 points?)
    # regionTooFew <- c("bretagne", "est humide", "poitou - façade atlantique", "sud-bassin parisien")
    critKeepNames <- c("notOneNA","notTooFew","within0") # "notOneNA","notTooFew","within20","RPGAllYears",
    # "NObsVigiOK" not relevant anymore: always true


#=================================
# Prép des Données
#=================================
source("generalDataBAYmin1.R") 
aOM <- get(load("aOM.rda")) # aOM, voir aussi allOrganismMetrics for other RPG/BDTOPO metrics and vigiculture points without RPG
# aOM <- get(load("allOrganismMetrics.rda"))
aOM$csa <- paste0(aOM$campagne,aOM$segArvSysteme)
aOM$cgr <- paste0(aOM$campagne,aOM$grandeRegion)




# check the distribution of metricModelLow
if(FALSE){
    par(mfrow=c(2,1))
    draw <- rpois(dim(aOM)[1],lambda=mean(aOM$metricModelLow,na.rm=TRUE))
    rg <- range(c(aOM$metricModelLow,draw),na.rm=TRUE)
    hist(aOM$metricModelLow,breaks=seq(rg[1]-0.5,rg[2]+0.5))
    hist(draw,breaks=seq(rg[1]-0.5,rg[2]+0.5))
    #=> clairement queue trop longue et excès de 0 pour aOM par rapport à la loi de poisson

    pos <- aggregate(aOM$metricModelLow,by=list(coordsAsString=aOM$coordsAsString,
                                                campagne=aOM$campagne,
                                                culture=aOM$culture),
                     function(x){length(which(x>0))})

    tot <- aggregate(aOM$metricModelLow,by=list(coordsAsString=aOM$coordsAsString,
                                                campagne=aOM$campagne,
                                                culture=aOM$culture),
                     function(x){length(x)})
    par(mfcol=c(2,2))
    dTheor <- rpois(dim(pos)[1],lambda=mean(pos$x))
    xs<-hist(pos$x,breaks=seq(-0.5,max(c(pos$x,dTheor))+0.5,1))
    hist(dTheor,breaks=xs$breaks)
    #=> encore un excès de zéros, mais queue ok, le problème c'est que si on élimine tous ceux qui n'ont aucune
    #   notation on risque d'éliminer trop de zéros, un risque à prendre?
    dTheor <- rpois(dim(tot)[1],lambda=mean(tot$x))
    xs<-hist(tot$x,breaks=seq(-0.5,max(tot$x,dTheor)+0.5,1))
    hist(dTheor,breaks=xs$breaks)

    # structure des tout zéros par culture
    table(tot$culture,tot$x)
    #                                1    2    4    7    8   14   21   35   70
    # Blé tendre d'hiver             2    0    0 3227    4   25    2    0    0
    # Colza                          0    0    0 3541    0   27    4    1    1
    # Maïs grain et Maïs fourrage 2027    6    0    0    0    0    0    0    0
    # Pomme de terre               630    2    1    0    0    0    0    0    0
    #=> clairement il y a quelques doublements de points (encore des ID_PLOT en double?)
    #   aussi bizarre qu'il puisse y avoir 1 ou 8 sur blé?!? cela veut dire qu'il y a 
    #   pas tous les bio-agresseurs à chaque fois, vu la taille du pb on s'inquiète pas plus que ça
    #   mais il faudrait les retirer: pb quoi retirer ?
    pos <- ChangeNameCol(pos,"x","pos")
    tot <- ChangeNameCol(tot,"x","tot")
    tot <- merge(tot,pos)
    table(tot$tot,tot$pos)
    #=> structure pas très différente pour les points doublés
    #   dans le doute on les flag et on fait sans eux
    table(tot$culture,tot$tot)
    #=> en fait, largement plus un problème avec le tri réalisé maintenant à l'import dans les observations

    tot$NObsVigiOK <- !((tot$culture %in% c("Blé tendre d'hiver") & tot$tot !=8) |
                        (tot$culture == "Colza"& tot$tot !=7) |
                        (tot$culture %in% c("Maïs grain et Maïs fourrage","Pomme de terre") & tot$tot !=1))
    aOM <- merge(aOM,tot[,c("coordsAsString","campagne","culture","NObsVigiOK")])

    par(mfcol=c(2,2))
    totNOk <- tot[which(tot$NObsVigiOK),]
    randTot <- rpois(dim(totNOk)[1],lambda=mean(totNOk$pos))
    xs<-hist(totNOk$pos,breaks=seq(-0.5,max(c(totNOk$pos,randTot))+0.5,1))
    hist(randTot,breaks=xs$breaks)
}
#=> pour l'instant on s'assure que l'on a bien tous les bio-agresseurs de la culture étudiés sur tous les points

## TODO: Should also flag the bio-agressors with too few observations

# for mec analysis
ANtoN <- read.csv("ANtoN.csv",check.names=FALSE,header = T)[,-1]
ANtoN$subject <- paste0(tolower(substring(ANtoN[,"subject"],1,1)),substring(ANtoN[,"subject"],2,20))
ANtoN$subject <- gsub("rouille-Jaune","rouilleJaune",ANtoN$subject)

multiDBStatsTemp <- NULL
AllModels <- function(organism,modelName,metricModel,...){
# for(organism in organismList$organism){
    cat("==========================================\n")
    cat("organisme:",organism,"\n")
    cat("==========================================\n")
    # information selection in organismList
    rowOrganism <- grep(paste0(organism),organismList$organism)
    datOrganism <- organismList[rowOrganism,]
    g <- datOrganism$grpeCult
    sub <- datOrganism$subject

    OM <- aOM[which(aOM$subject==organism),]
    iRPGSame <- grep("RPGSame_w.*_area",colnames(OM))
    iRPGSameNo10000 <- grep("RPGSame_w[0-9]{1,4}_area",colnames(OM),perl=TRUE)
    iRPGPrec <- grep("RPGPrec_w.*_area",colnames(OM))
    iRPGPrecNo10000 <- grep("RPGPrec_w[0-9]{1,4}_area",colnames(OM),perl=TRUE)

    iBDTOPOnoVeg <- grep(paste0("BDTOPO",yearVeg,"[^v].*_w.*_area"),colnames(OM))
    iBDTOPOboisHaie <- grep(paste0("BDTOPO",yearVeg,"(bois|haie)_w.*_area"),colnames(OM)) #only 0 and NA
    iBDTOPOall <- grep(paste0("BDTOPO",yearVeg,"(bois|haie|lande|boisTraites)_w.*_area"),colnames(OM)) #only 0 and NA 

    iBDTOPOGetBoisHaie <- grep(paste0("BDTOPO",yearVeg,"Get.*[^I][^G][^N]_w.*_area"),colnames(OM))# bois haie manual 
    iBDTOPOGetBoisHaieNo10000 <- grep(paste0("BDTOPO",yearVeg,"Get.*[^I][^G][^N]_w[0-9]{1,4}_area"),colnames(OM))

    iBDTOPOGetAllIGN <- grep(paste0("BDTOPO",yearVeg,"Get.*IGN_w.*_area"),colnames(OM)) # all auto IGN
    iBDTOPOGetAllIGNNo10000 <- grep(paste0("BDTOPO",yearVeg,"Get.*IGN_w[0-9]{1,4}_area"),colnames(OM)) 

    iBDTOPOGetBoisHaieIGN <- grep(paste0("BDTOPO",yearVeg,"Get(Foret|Haie)IGN_w.*_area"),colnames(OM)) # bois haie auto IGN
    iBDTOPOGetBoisHaieIGNNo10000 <- grep(paste0("BDTOPO",yearVeg,"Get(Foret|Haie)IGN_w[0-9]{1,4}_area"),colnames(OM))

    iBDTOPOGetBoiTraitesIGN <- grep(paste0("BDTOPO",yearVeg,"GetBoisTraitesIGN_w.*_area"),colnames(OM)) # group of boisTraites IGN
    iBDTOPOGetBoiTraitesIGNNo10000 <- grep(paste0("BDTOPO",yearVeg,"GetBoisTraitesIGN_w[0-9]{1,4}_area"),colnames(OM)) 

    iBDTOPOGetPVVIGN <- grep(paste0("BDTOPO",yearVeg,"Get(Vigne|Verger|Peupleraie)IGN_w.*_area"),colnames(OM)) # boisTraies split IGN
    iBDTOPOGetPVVIGNNo10000 <- grep(paste0("BDTOPO",yearVeg,"Get(Vigne|Verger|Peupleraie)IGN_w[0-9]{1,4}_area"),colnames(OM)) 

    iBDTOPOGetLandeIGN <- grep(paste0("BDTOPO",yearVeg,"Get.*Lande.*IGN_w.*_area"),colnames(OM))
    iBDTOPOGetLandeIGNNo10000 <- grep(paste0("BDTOPO",yearVeg,"Get.*Lande.*IGN_w[0-9]{1,4}_area"),colnames(OM))

    #iBDTOPOGetLande, GetVigne, GetVerger


    iPC <- grep(paste0("^PC[0-9]+$"),colnames(OM),perl=TRUE)
    iPC1_2 <- grep(paste0("^PC[1-2]+$"),colnames(OM),perl=TRUE)
    iPrairiesSame <- grep("PrairiesSame.*_w.*_area",colnames(OM))
    iPrairiesSameNo10000 <- grep("PrairiesSame.*_w[0-9]{1,4}_area",colnames(OM))
    iRatioCult <- grep("^ratioCult$",colnames(OM)) # to include the ratio of the culture 
                                                    # in the ilots over the years
    iRPGproxRot <- grep("^RPGproxRot$",colnames(OM))
    iRPGproxPrairie <- grep("^RPGproxPrairie$",colnames(OM))
    iRPGproxRot3 <- grep("^RPGproxRot3$",colnames(OM))
    iRPGproxPrairie3 <- grep("^RPGproxPrairie3$",colnames(OM))
    iColReg <- eval(parse(text=paste0("c(",metricsGroup,")")))
    #=> des trucs bizarre (genre fort effet des haies à 10000 sur les maladies...)
    #   si on utilise iBDTOPO2012boisHaie au lieu de iBDTOPO2012noVeg
    #   ou un effet dilution la même année pour la rouille jaune à 200 m ?!?
    #   les haies favorisent les pucerons d'automne aussi, assez fortement (attendu)
    #   sinon ne change pas grand chose

    # Il faut : - faire un graphique résumé
    #           - essayer avec segments arvalis au lieu des régions
    #           - remplacer segment arvalis par paramètre acp sur le paysage sur les segments arvalis ou sur rayon 10km

    # rowReg <- which(apply(OM[,iColReg],1,function(vect){all(!is.na(vect))}))
    multiTemp <- NULL
    for (iColMetric in 1){
        allUsedCols <- c(metricModel,names(OM)[iColReg],factorNames)

        notOneNA <- apply(OM[,allUsedCols],1,function(vect){!anyNA(vect)})
        regionTooFewWithProvence <- c(regionTooFew,"provence")
        notTooFew <- ! OM$grandeRegion %in% regionTooFew
        notTooFewWithoutProvence <- ! OM$grandeRegion %in% regionTooFewWithProvence

        within0 <- OM$RPGwithin0 # RPG not corresponding
        NObsVigiOK <- OM$NObsVigiOK # weird things on Vigiculture (n != n bio-agressors)
        RPGAllYears <- OM$nYearsDoc_2007.2014 == 8 # have RPG for all the years

        critKeep <- paste0(critKeepNames,collapse="&")
        rowReg <- eval(parse(text=paste0("which(",critKeep,")")))
        rowRegWithoutProvence <- which(notOneNA & notTooFewWithoutProvence)

        # Variables sélection
        matParameters <- OM[rowReg,iColReg]
        # # only grandeRegion as factor
        # matFactors <- model.matrix(OM[rowReg,metricModel]~factor(OM[rowReg,"grandeRegion"]))[,-1]

        # for all declared factors
        factors <- paste0("factor(OM[rowReg,\"",factorNames,"\"])",collapse="+")
        OM$segArvSysteme <- paste0("segArv_",OM$segArvSysteme)
        OM$campagne <- paste0("y_",OM$campagne)
        matFactors <- eval(parse(text=paste0("model.matrix(OM[rowReg,metricModel]~",factors,")[,-1,drop=FALSE]")))
        # summary(OM[,allUsedCols]) # to check if something wrong

        colnames(matFactors) <- gsub("^.*])","",colnames(matFactors))
        y <- OM[rowReg,metricModel]
        X <- as.matrix(data.frame(matParameters,matFactors))
        colnames(X) <- gsub("X\\.|\\.\\.","",colnames(X),perl=TRUE)

        # create temporary db
        organismExplicativeVariables <- X
        multiDBStats <- matrix(ncol=length(colStatsSummary),nrow=length(colnames(organismExplicativeVariables)))
        multiDBStats <- data.frame(multiDBStats)
        colnames(multiDBStats) <- colStatsSummary

        # fill specification names
        multiDBStats$idModel <- paste0(modelName,"_",colnames(organismExplicativeVariables),"_",metricModel)
        multiDBStats$echelle <- gsub("^.*_w([0-9]+)_.*$","\\1",colnames(organismExplicativeVariables),perl=TRUE)
        multiDBStats$subject <- datOrganism$subject
        temp <- gsub(paste0("RPG|BDTOPO",yearVeg),"", colnames(organismExplicativeVariables),perl=TRUE) 
        multiDBStats$leverStats <- gsub("^(.*)_w[0-9]+_.*$","\\1",temp,perl=TRUE)

        multiDBStats$partialCorrelation <- NA

        # # making other similar temporary db
        # multiDBStatsNorm <- multiDBStats
        # multiDBStatsNorm$idModel <- paste0("multivarieNorm_",colnames(organismExplicativeVariables),"_",metricModel)
        # DBstatsLassoNormMin <- multiDBStats
        # DBstatsLassoNormMin$idModel <- paste0("LassoNormMin_",colnames(organismExplicativeVariables),"_",metricModel)

        # variables transformation
        Xglm <- model.matrix(y~X)

        Xnorm <- apply(X,2,AutoScale)
        XnormGlm <- model.matrix(y~Xnorm)

        XlogNorm <- apply(X,2,AutoScaleLog)
        XLogNormGlm <- model.matrix(y~XlogNorm)
        XLog <- apply(X,2,Log)
        XLogGlm <- model.matrix(y~XLog)

        dat <- data.frame(cbind(y,XlogNorm))
        dat <<- dat
        datNoY <- dat[,-grep("^y$",names(dat))]
        datNoY <<- datNoY
        cat("dim dat:",dim(dat),"\n")

        # lasso
        # mod <- glmnet(y=y,x=X,family="poisson")
        # modNorm <- glmnet(y=y,x=XNorm,family="poisson")
        # billboard <- LassoTreeSummaryBis(mod)
        # billboardNorm <- LassoTreeSummaryBis(modNorm)
        # meanCoefs <- apply(mod$beta[names(billboard),1:20],1,mean)
        # meanCoefsNorm <- apply(modNorm$beta[names(billboardNorm),1:20],1,mean)
        # tableBillCoefs <- data.frame(cbind(billboard,meanCoefs,meanCoefsNorm))
       
        if(modelName=="LassoNormMin"){
            modMultiOrganism <- LassoPvalGlm(y,XLogNormGlm,lambda="lambda.1se",...)
        }else if(modelName =="LassoNormMinPA"){
            modMultiOrganism <- LassoPvalGlm(y>0,XLogNormGlm,lambda="lambda.1se",family="binomial",...)
        }else if(modelName =="LassoNormMinCox"){
            var <- "Median"
            # yCox <- cbind(time=,status=)
            modMultiOrganism <- LassoPvalGlm(yCox,XLogNormGlm,lambda="lambda.1se",family="cox",...)
        }else if(modelName =="LassoNormMinBin"){
            nObs <- OM[rowReg,"NObs"]
            yBin<- cbind(nObs-y,y)
            modMultiOrganism <- LassoPvalGlm(yBin,XLogNormGlm,lambda="lambda.1se",family="binomial",...)
           # browser()
        }else if(modelName =="LassoMinBin"){
            nObs <- OM[rowReg,"NObs"]
            yBin<- cbind(nObs-y,y)
            modMultiOrganism <- LassoPvalGlm(yBin,XLogGlm,lambda="lambda.1se",family="binomial")
            # predict(attr(modMultiOrganism,"mod"),newx=XLogGlm,s=modMultiOrganism$lambda.1se)
            attr(modMultiOrganism,"X") <- XLogGlm
        }else if(modelName =="LassoNormMinGaus"){
            modMultiOrganism <- LassoPvalGlm(y,XLogNormGlm,lambda="lambda.1se",family="gaussian",...)
        }else if(modelName =="glmNorm"){
            dat <- data.frame(cbind(y,Xnorm))
            mod <- glm(y~.,data=datNoY)
            modMultiOrganism <- summary(mod) # ça a le mérite d'être rapide
            est <- data.frame(modMultiOrganism$coefficients,check.names=FALSE)
            library("rsq")
            outPCor <- try(pcor(mod))
            est$partialCorrelation <- NA
            if(class(outPCor)!="try-error"){
                iVarInEst <- match(outPCor$variable,row.names(est))
                iNotNA <- which(!is.na(iVarInEst))
                expect_equal(outPCor$variable[iNotNA],row.names(est)[iVarInEst[iNotNA]])
                est$partialCorrelation[iVarInEst[iNotNA]] <- outPCor$partial.cor[iNotNA]
            }
            modMultiOrganism$coefficients <- as.matrix(est)
        }

        # # essayer de sortir quelque chose à partir simplement de glmnb ? 
        if(modelName=="glmnb"){
            library("MASS")
            mod <- glm.nb(y~.,data=dat)
            modMultiOrganism <- summary(mod) # ça a le mérite d'être rapide
            est <- data.frame(modMultiOrganism$coefficients,check.names=FALSE)
            library("rsq")
            outPCor <- try(pcor(mod))
            est$partialCorrelation <- NA
            if(class(outPCor)!="try-error"){
                iVarInEst <- match(outPCor$variable,row.names(est))
                iNotNA <- which(!is.na(iVarInEst))
                expect_equal(outPCor$variable[iNotNA],row.names(est)[iVarInEst[iNotNA]])
                est$partialCorrelation[iVarInEst[iNotNA]] <- outPCor$partial.cor[iNotNA]
            }
            modMultiOrganism$coefficients <- as.matrix(est)
        }else if(modelName=="glmBinomial"){
            mod <- glm(cbind(y,OM$NObs[rowReg]-y)~.,data=datNoY,family="binomial")
            modMultiOrganism <- summary(mod)
            est <- data.frame(modMultiOrganism$coefficients,check.names=FALSE)
            library("rsq")
            outPCor <- try(pcor(mod))
            est$partialCorrelation <- NA
            if(class(outPCor)!="try-error"){
                iVarInEst <- match(outPCor$variable,row.names(est))
                iNotNA <- which(!is.na(iVarInEst))
                expect_equal(outPCor$variable[iNotNA],row.names(est)[iVarInEst[iNotNA]])
                est$partialCorrelation[iVarInEst[iNotNA]] <- outPCor$partial.cor[iNotNA]
            }
            modMultiOrganism$coefficients <- as.matrix(est)
        }else if(modelName=="glmPA"){
            yPA <- y>0
            datNoY <- dat[,-grep("^y$",names(dat))]
            mod <- glm(yPA~.,data=datNoY,family="binomial")

            modMultiOrganism <- summary(mod)
            est <- data.frame(modMultiOrganism$coefficients,check.names=FALSE)
            library("rsq")
            outPCor <- try(pcor(mod))
            est$partialCorrelation <- NA
            if(class(outPCor)!="try-error"){
                iVarInEst <- match(outPCor$variable,row.names(est))
                iNotNA <- which(!is.na(iVarInEst))
                expect_equal(outPCor$variable[iNotNA],row.names(est)[iVarInEst[iNotNA]])
                est$partialCorrelation[iVarInEst[iNotNA]] <- outPCor$partial.cor[iNotNA]
            }
            modMultiOrganism$coefficients <- as.matrix(est)
        }else if(grepl("glmer",modelName)){
            namesParam <- names(OM)[iColReg]
            allPossibleFactors <- setdiff(names(OM),namesParam)
            dat <- cbind(dat[,c("y",namesParam),drop=FALSE],OM[rowReg,allPossibleFactors,drop=FALSE])
            params <- paste0(names(matParameters),collapse="+")
            if(length(factorNames)>0){
                fixFactors <- paste0("+",factorNames,collapse="")
            }else{
                fixFactors <- ""
            }
            if(modelName=="glmer.nb"){
                formula <- paste0("y~",params,fixFactors,glmerRE)
                mod <- glmer.nb(eval(parse(text=formula)),data=dat)
            }else if(modelName=="glmerBin"){
               # browser()
                dat$NNeg <- OM$NObs[rowReg]-y
                formula <- paste0("cbind(dat$y,dat$NNeg)~",params,fixFactors,glmerRE)
                mod <- glmer(eval(parse(text=formula)),data=dat,family="binomial")
            }
            modMultiOrganism <- summary(mod)
            est <- data.frame(modMultiOrganism$coefficients,check.names=FALSE)
            est$partialCorrelation <- NA
            modMultiOrganism$null.deviance <- NA
            modMultiOrganism$deviance <- modMultiOrganism$AICtab[["deviance"]]
            modMultiOrganism$aic <- modMultiOrganism$AICtab[["AIC"]]
        }

        # # # lasso negative binom
        # library("mpath") # lasso +++, basé sur glmnet
        # library("pscl") # zeroinfl
        # # hist(y,breaks=30)

        # # # idéalement, mais très long et apparemment pb de convergence:
        # # modZINBNorm<-zipath(y~.|1,data=dat,family="negbin")
        # # modZINBNorm<-cv.zipath(y~.|1,data=dat,family="negbin",n.cores=8)

        # # # quand même très long sur altise (pas fini)
        # b <- cv.glmregNB(y~.,data=dat,n.cores=4) # paraît raisonnable et ça prend pas trop de temps
        #                                           # par contre beaucoup de warnings
        # # beta_lambda_min <- coef(b) 
        # # beta_lambda_min[which(beta_lambda_min!=0)]
        # # #=> résultats plutôt sympa sauf que ça correspond au lambda.min
        # i_lambda_1se <- min(which(b$cv>b$cv[b$lambda.which]-b$cv.error[b$lambda.which]))-1
        # beta_lambda_1se <- b$fit$beta[,i_lambda_1se]
        # modLassoNB <- beta_lambda_1se[which(beta_lambda_1se!=0)]
        # #=> à part prairies same 1000 - et bois 1000+5000 (et haie 5000 suivant les cas) on perd tout

        # b <- cv.glmregNB(y~.,data=dat,weights=OM$NObs[rowReg],n.cores=4)
        # #=> ajout des poids=> plutôt perte de choses pas grand chose

        # dat <- data.frame(cbind(y=OM$meanObs[rowReg],XlogNorm))
        # b <- cv.glmregNB(y~.,data=dat,weights=OM$NObs[rowReg],n.cores=4)
        
        # ## mechanism analysis
        # # define what we want to work on
        # LandUseInMetrics <-c("BDTOPO2012bois","BDTOPO2012haie","PrairiesSame","RPGPrec","RPGSame")
        # 
        # # check
        # nExperts <- ANtoN[which(ANtoN$subject==organism),"NumberOfExpert"]
        # expect_true(all(nExperts==nExperts[1]))
        # nExperts <- nExperts[1]
        # 
        # # hereafter could merge mechanisms as necessary and keep only the ones we want to put in the regression
        # isSureMechanisms <- ! grepl("?",names(ANtoN), fixed = TRUE)
        # isMechanism <- grepl("{.*}",names(ANtoN),perl = TRUE)
        # keptMechanisms <- names(ANtoN)[which(isMechanism & isSureMechanisms)]
        # # product to get surface per mechanism for each observation
        # # basic idea of calculation: C <- matrixB %*% matrixA
        # # select the parts that can be multiplied in ANtoN
        # if(any(ANtoN$subject == organism)==TRUE){
        #   keptLandUse <- c("bois","haies","prairies","Prec","Same")
        #   ANtoNsel <- ANtoN[which(ANtoN$subject == organism & ANtoN$lever %in% keptLandUse),]
        #   ANtoNsel <- ANtoNsel[match(ANtoNsel$lever,keptLandUse),keptMechanisms]
        #   
        #   # make nicer mechanisms names
        #   names(ANtoNsel) <- gsub("[{}]","",names(ANtoNsel))
        #   names(ANtoNsel) <- gsub("\\+","p",names(ANtoNsel))
        #   names(ANtoNsel) <- gsub("\\.\\-",".m",names(ANtoNsel))
        #   names(ANtoNsel) <- gsub("([0-9])-([0-9])","\\1to\\2",names(ANtoNsel))
        #   names(ANtoNsel) <- paste0("meca",names(ANtoNsel))
        #   
        #   Ainit <- as.matrix(ANtoNsel)
        #   AblockDiag <- bdiag(Ainit,Ainit,Ainit,Ainit)
        #   colnames(AblockDiag) <- paste0(rep(colnames(Ainit),4),rep(c("w200","w1000","w5000","w10000"),each=length(colnames(Ainit))))
        #   
        #   # select the parts that can be multiplied in B
        #   colsInOrder <- names(OM)[grep(paste0(LandUseInMetrics,"_w.*_area",collapse="|"),names(OM))]
        #   LandUse <- gsub("^(.*)_w.*","\\1",colsInOrder,perl=TRUE)
        #   ordering <- match(LandUse,LandUseInMetrics)
        #   colsInOrder <- colsInOrder[order(ordering)]
        #   # surface value B1: just surface, B2: S/buffer surface, B3: (S-mean)/sd
        #   B1 <- OM[,colsInOrder]
        #   # Surface 2 (B2)
        #   B2 <- B1
        #   buffers <- c("200","1000","5000","10000")
        #   for(buf in buffers){
        #     buf <- as.numeric(buf)
        #     B2[,grep(paste0(".*",buf,".*"),names(B2))] <- as.matrix(B2[,grep(paste0(".*",buf,".*"),names(B2))]/buf^2*pi)
        #   }
        #   # Surface 3 (B3)
        #   B3 <- B1
        #   for(buf in buffers){
        #     buf <- as.numeric(buf)
        #     names <- names(B3[,grep(paste0(".*",buf,".*"),names(B3))])
        #     for(n in names){
        #       mean <- mean(B3[,n],na.rm=TRUE)
        #       sd <- sd(B3[,n],na.rm=TRUE)
        #       normFun <- function(x){abs(x-mean)/sd}
        #       B3[,n] <- as.matrix(normFun(B3[,n]))
        #     }
        #   }
        #   # do the multiplication itself
        #   C1 <- as.matrix(B1)%*%as.matrix(AblockDiag)
        #   C2 <- as.matrix(B2)%*%as.matrix(AblockDiag)
        #   C3 <- as.matrix(B3)%*%as.matrix(AblockDiag)
        #   
        #   allDatMeca1 <- cbind(OM,C1)
        #   allDatMeca2 <- cbind(OM,C2)
        #   allDatMeca3 <- cbind(OM,C3)
        #   expect_equal(length(setdiff(names(AblockDiag),names(allDatMeca1))),0)
        #   expect_equal(length(setdiff(names(AblockDiag),names(allDatMeca2))),0)
        #   expect_equal(length(setdiff(names(AblockDiag),names(allDatMeca3))),0)
        #   
        #   # select the mechanisms that are usable (let's use surface2 for example)
        #   sumValsNotNA <- apply(allDatMeca2[,colnames(AblockDiag)],2,sum,na.rm=TRUE)
        #   analyzedMeca <- colnames(AblockDiag)[which(sumValsNotNA>0)]
        #   
        #   # select the observations that are usable
        #   rowReg <- which(!apply(allDatMeca2[,analyzedMeca],1,anyNA))
        #   
        #   modMeca1 <- glmnet(y=allDatMeca2[rowReg,grep(paste0(metricModel,"\\>"), colnames(allDatMeca2))],
        #                      x=as.matrix(allDatMeca2[rowReg,analyzedMeca]),
        #                      lambda.min.ratio=10e-20,
        #                      family="poisson",alpha = 1)
        #   
        #   modMeca1cv <- cv.glmnet(y=allDatMeca2[rowReg,grep(paste0(metricModel,"\\>"), colnames(allDatMeca2))],
        #                           x=as.matrix(allDatMeca2[rowReg,analyzedMeca]),
        #                           lambda.min.ratio=10e-30,
        #                           family="poisson",alpha = 1)
        #   cat("--------------------------------------------------------------\n")
        #   cat("Coefficient:","mechanism LASSO regression -surface proportion-","\n")
        #   if(all(coef(modMeca1cv)[-1]==0)==TRUE){
        #     cat("no mechanism correlation is observed","\n")
        #   }else{print(coef(modMeca1cv))}
        #   }
        # fill multivariate model results database
        # modMultiOrganism <- summary(glm(y~Xglm,family="poisson")) # 

        # modNormMultiOrganism <- summary(glm.nb(y~XnormGlm))
        # # AIC(glm(cbind(y,OM$NObs[rowReg]-y)~Xglm,family="binomial"))
        # ShortSummary(glm(y~XLogNormGlm))
        # ShortSummary(glm.nb(y~XLogNormGlm,weights=OM$NObs[rowReg])) # le meilleur AIC mais ne converge pas bien pour les méligèthes
        # # => changement majeur si on ajoute les NObs comme poids
        # #    en fait correspond à multiplier les points par les poids...
        # ShortSummary(glm(cbind(y,OM$NObs[rowReg]-y)~XLogNormGlm,family="binomial")) # similaire aux autres pour les meligethes

        # # utilisation de meanObs
        # ShortSummary(glm.nb(OM$meanObs[rowReg]~XLogNormGlm,weights=OM$NObs[rowReg])) # similaire aux autres pour les meligethes
        # #=> tout à fait raisonnable, sans NObs pas vraiment plus d'info, par contre si on ajoute NObs 
        # #   dans les weights ça change tout en termes de significativité (mais plein de warnings)

        # # quantile regression ? 

        # # simple negative binomial zero-inflated
        # modZINBNorm<-zeroinfl(y~.|1,data=dat,dist="negbin",trace=TRUE) # AIC ~ glm.nb mais converge
        # print(summary(modZINBNorm))
        # browser()
        # #=> visualisation un par un, effet dilution régulièrement fort 
        # #   mais aussi des difficultées avec plusieurs échelles significatives en sens contraires

        FillDBStats <- function(mod,DBStats,explicativeVariables){
            est <- mod$coefficients
            # find the radical in row.names
            sameLetter <- TRUE
            iEndSame <- 0
            while(sameLetter){
                sameLetter <- length(setdiff(substr(row.names(est)[-1],1,iEndSame+1),""))==1
                if(sameLetter){
                    iEndSame = iEndSame+1
                }
            }
            
            radicalNameVar <- substr(row.names(est)[2],1,iEndSame)
            # extract variables estimates
            est <- data.frame(est,check.names=FALSE)
            for(nameVariable in colnames(explicativeVariables)){
                # cat("nameVariable:",nameVariable,",")
                patternVar <- paste0("^",radicalNameVar,nameVariable,"$")
                iVarInEst <- grep(patternVar,rownames(est))
                if(length(iVarInEst)>1){browser()}
                valsToKeep <- est[iVarInEst,,drop=FALSE]
                if(dim(valsToKeep)[1] > 0){
                    # valsToKeep <- rbind(valsToKeep,rep(NA,dim(valsToKeep)[2]))
                    iVariable <- grep(paste0("_",nameVariable,"_"),DBStats$idModel)
                    cols <- match(c("Estimate", "Std. Error", "z value","Pr(>|z|)"),colnames(valsToKeep))
                    if(any(is.na(cols))){
                        cols <- match(c("Estimate", "Std. Error", "t value","Pr(>|t|)"),colnames(valsToKeep))
                    }
                    DBStats[iVariable,c("estimate","std.error","z.value","p.value")] <-valsToKeep[,cols]

                    if(!is.null(est$partialCorrelation)){
                        DBStats$partialCorrelation[iVariable] <- est[iVarInEst,"partialCorrelation"]
                    }else if("devWoCoef" %in% colnames(est) & !is.na(deviance(mod))){
                        pR2 <- 1-deviance(mod)/valsToKeep[,"devWoCoef"]
                        DBStats$partialCorrelation[iVariable] <- sqrt(pR2)*sign(DBStats[iVariable,"estimate"])
                    }
                }
            }

            # add model fitting quality
            DBStats$aic <- mod$aic
            DBStats$deviance <- mod$deviance
            DBStats$null.deviance <- mod$null.deviance
            if(is.null(mod$D2)){
                DBStats$D2 <- 1-mod$deviance/mod$null.deviance
            }else{
                DBStats$D2 <- mod$D2
            }
            if(is.null(mod$D2cor)){
                DBStats$D2cor <- NA
            }else{
                DBStats$D2cor <- mod$D2cor
            }
          
            DBStats$family <- if(class(mod$family)=="character"){mod$family}else{mod$family$family}
            return(DBStats)
        }
        multiDBStats <- FillDBStats(modMultiOrganism,multiDBStats,organismExplicativeVariables) 
        # multiDBStatsNorm <- FillDBStats(modNormMultiOrganism,multiDBStatsNorm,organismExplicativeVariables) 
        # DBstatsLassoNormMin <- FillDBStats(modLassoNormMin,DBstatsLassoNormMin,organismExplicativeVariables)
        # DBstatsLassoNormMin <- FillDBStats(modLassoNB,DBstatsLassoNormMin,organismExplicativeVariables)

        multiTemp <- rbind(multiTemp,multiDBStats) # multiDBStatsNorm,DBstatsLassoNormMin) # ,
    }
    organismName <- organism
    attr(multiTemp,"allUsedCols") <- allUsedCols
    attr(multiTemp,"mod") <- modMultiOrganism
    attr(multiTemp,"organismName") <- organismName
    return(multiTemp)
}

#multiDBStatsTemp <- AllModels("meligethe",modelName=modelName,metricModel=metricModel,  bootstrapVarSelect=10,bootstrapVals=10)

library("parallel")
multiDBStatsTemp <- mclapply(organismList$organism,AllModels,modelName=modelName,metricModel=metricModel,mc.cores=8,bootstrapVarSelect=100, bootstrapVals=100)

modMultiOrganism <- attr(multiDBStatsTemp[[1]],"mod")
# mod <- attr(modMultiOrganism,"mod")
# predict(mod,newx=mod$beta[1:10,],s=modMultiOrganism$lambda.1se)

# multiDBStatsTemp <- lapply(organismList$organism,AllModels,modelName=modelName,metricModel=metricModel)
save(multiDBStatsTemp,file="multiDBStatsTemp.rda")

allUsedCols <- attr(multiDBStatsTemp[[1]],"allUsedCols")
library("data.table")
multiDBStatsFin <- rbindlist(multiDBStatsTemp)
save(multiDBStatsFin,file="multiDBStatsFin.rda")

multiDBStatsFin$leverStats <- gsub("GetForet.*","bois",multiDBStatsFin$leverStats)
multiDBStatsFin$leverStats <- gsub("GetHaie.*","haie",multiDBStatsFin$leverStats)

multiDBStatsFin <- merge(multiDBStatsFin,correspondancesStatsElicitLevers,by="leverStats",all.x=TRUE)
multiDBStatsFin$lever.buffer <- paste0(multiDBStatsFin$elicitation, multiDBStatsFin$echelle)


save(multiDBStatsFin,file="multiDBStatsFin.rda")


# # fairly linear relationship between partial correlation and lasso estimate on normalized variables
# with(multiDBStatsFin[grep(modelName,multiDBStatsFin$idModel),],plot(estimate,partialCorrelation))
# abline(a=0,b=1)

# to visualize a model: 
unique(multiDBStatsFin$subject)
# [1] Sclerotinia   Rouille-Jaune PuceronEpis   Cecidomyie    Altise       
# [6] Phoma         Septoriose    Pyrale        Meligethe  
#=> What selector on subject can be used
multiDBStatsFin <- multiDBStatsFin[order(multiDBStatsFin$echelle),]
for(rav in organismList$subject){
    iModel <- which(grepl(paste0(modelName,"_.*"),multiDBStatsFin$idModel)& 
                    grepl(rav,multiDBStatsFin$subject)&
                    multiDBStatsFin$estimate!=0)
    print(multiDBStatsFin[iModel,c("subject","leverStats","echelle","estimate","partialCorrelation")])
}
# table résumé des observations :
# - nombre d'observations positives étudiées 
used <- aOM[which(!apply(aOM[,allUsedCols],1,anyNA)),]
# table(used$subject,useNA="always",used$metricModelLow>0)
# note: depends on the exact choice of rowReg...
#                 FALSE TRUE <NA>
# altise           2246  787    0
# cecidomyie       2042  255    0
# charanconBT      1958 1075    0
# charanconSil     2918  115    0
# charanconTige    1300 1733    0
# meligethe        2430  603    0
# mildiou           366   98    0
# phoma            2307  726    0
# pietinEchaudage  2247   54    0
# puceronAutomne   2096  201    0
# puceronEpis      2018  279    0
# pyrale           1271   59    0
# rouilleBrune     2061  236    0
# rouilleJaune     2071  226    0
# sclerotinia      2654  379    0
# septoriose       1291 1006    0
#=> tout est au dessus de 30 mais mildiou, pyrale, pietinEchaudage ont <100 observations positives

# available <- aOM[,c("subject","RPGwithin20",allUsedCols)]
# table(available$subject,available$ratioCult>0,useNA="always")
# #                 FALSE TRUE <NA>
# # altise            264 2781  577
# # cecidomyie         89 2229  969
# # charanconBT       264 2781  577
# # charanconSil      264 2781  577
# # charanconTige     264 2781  577
# # meligethe         264 2781  577
# # mildiou            62  402  174
# # phoma             264 2781  577
# # pietinEchaudage    90 2232  971
# # puceronAutomne     89 2229  969
# # puceronEpis        89 2229  969
# # pyrale            202 1138  699
# # rouilleBrune       89 2229  969
# # rouilleJaune       89 2229  969
# # sclerotinia       264 2781  577
# # septoriose         89 2229  969

source("generalMEDBAYmin1.R")
source("makeHistBilanBAYmin1.R")
