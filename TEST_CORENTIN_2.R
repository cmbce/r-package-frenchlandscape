library("frenchLandscape") # Pour le NumTo3Char et bien d'autres choses
library("rgeos")

metrics <- list()
colza <- ExtLoad("bioagresseurs","culture/vigiculture_colza")
ble <- ExtLoad("bioagresseurs","culture/vigiculture_ble")
mais <- ExtLoad("bioagresseurs","culture/vigiculture_mais")
pointsRef <- rbind(ble,colza,mais)
pointsRef <- pointsRef[which(pointsRef$codeDepartement == 18),]
for(shapesSelector in 15){
    cat("shapesSelector:",shapesSelector,"\n")
    for(year in 2011){
        metrics[[as.character(year)]] <- ExtractMetrics(pointsRef,"RPG",year,shapesSelector=shapesSelector,
                                                        widths=c(200,1000,5000,10000),
                                                        vectShortNames=c("minDistance","count","perimeter","area"),
                                                        nCores=8,useCache=TRUE)
    }
}

