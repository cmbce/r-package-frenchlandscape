library("RPG")
library("bioagresseurs")
library("frenchLandscape")
library("testthat")

# for all points in colza, get the corresponding polygons, do an operation and return the result
# for now, assuming that there 
extract <- function(pts,bd,layer,year,fn){
    browser()
    # for each  departement
    # split points per departement and load the corresponding files, select the useful polygons and apply the measure


}
data(parcelle_colza)
#~extract(parcelle_colza,"RPG",
pts <- colza
baseName <- "RPG"
library(sp)
numDeps <- as.factor(as.numeric(as.character(over(pts,frenchDepartements)$CODE_DEPT)))
expect_equal(length(which(is.na(numDeps))),0)
# for(iLevel in 1:length(levels(numDeps))){
iLevel <- 1
    # departements to load
    numDep <- levels(numDeps)[iLevel]
    iPts <- which(numDeps == numDep)
    depsToLoad <- DepartementsToLoad(pts[iPts,],500)
    
    # years to Load
    numYears <- unique(pts[iPts,]$Campagne)
    # for(year in numYears){
        year <- numYears[1]
        if(baseName == "RPG"){
            ilots <- LoadDepartements(depsToLoad,baseName,year,"ilots")
            cult <- LoadDepartements(depsToLoad,baseName,year,"ilotsCult")
        }
        # keep only colza in RPG

        # check match pts colza with ilots/cult == colza
        if(EquivalentProj(pts,ilots)){
            try(proj4string(pts) <- proj4string(ilots),silent=T)
        }
        ID_ILOTS <- over(pts,ilots)

    # }
#~}




