
########################
# 
# Script: make_Data_BAYmin1.R 
#
# Purpose: Make table for generalModels with pressure, landscape, meteo, prev year inoculum variables 
#
# Details:To be used by generalModels_BAYmin1, cover (2009 2014) 
#
# Author: Thomas Delaune (thomas.delaune.inra@gmail.com)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

library(dplyr)
library(reshape2)
library(ggplot2)
library(frenchLandscape)
library(testthat)
library(stringr)
library(DataManagement) 
source("statsFns.R") 

source("generalDataBAYmin1.R") 

'%nin%' <- Negate('%in%') 
CheckLaggedValue  <- function(groupvector, timevector,respvar, respvarlagged) {
       for (ind in unique(allLinesDP$org_dept)) { 
           for (y in c((min(unique(timevector))+1):max(unique(timevector)))) {
               i=which(groupvector == ind & timevector == y, arr.ind = T)
               j=which(groupvector == ind & timevector == (y-1), arr.ind = T)
               year = respvarlagged[i] 
               prevyear = respvar[j]
               if(year %nin%  prevyear){stop("the lag is not correct")}
           }
       }
}

#load(file.path(GetDicoPath("bioagresseurs"),"Pression_departementale","Data_rdt_ba.rda"))
load(file.path(GetDicoPath("pkgsFolder"),"r-package-frenchlandscape","aOM.rda"))

dat_pression <- aOM %>% select(campagne, codeDepartement, subject, NObs, nAboveMedMoy) %>%
     group_by(campagne, codeDepartement, subject) %>% 
     summarise(sum_nAbove = sum(nAboveMedMoy, na.rm = T),
               sum_NObs = sum(NObs, na.rm = T)) %>% 
     ungroup() %>% 
     mutate(MedMoy_Dept_Same = sum_nAbove/sum_NObs) 
#Normalize, avoid Nan in partial correlation  ?? 
#dat_pression$MedMoy_Dept_Same <- apply(dat_pression[,"MedMoy_Dept_Same"], 2, AutoScale) 
###METEO###

#SameNameDept <- function(x){
#    out <- tolower(x)
#    out <- gsub("é","e",out)
#    out <- gsub("è","e",out)
#    out <- gsub("ô","o",out)
#    out <- gsub("territoire","belfort",out)
#    out <- gsub("belfort-de-belfort","belfort",out)
#    return(out)
#}
#load(file.path(GetDicoPath("RPG"), "..", "Agreste", "Data_rdt_1989_2016.rda")) 
#load(DonneesR( "MeteoFrance", "data_clim_fr2.rda"))
#
#
## Getting a intermediate dataset for code dept and name dept 
#data_dept <- Data_rdt_1986_2016 %>% select(geographie, code_dept) 
#data_dept$name_dept <-substr(data_dept$geographie,6,nchar(data_dept$geographie))
#data_dept <- unique(data_dept) %>% rename(codenameDepartement = geographie, codeDepartement = code_dept, nameDepartement = name_dept)
#
#
## Integration of MeteoFrance data aggregated departement/year 
#data_clim <- data_clim_fr2 %>% 
#    rename(campagne = year_harvest, nameDepartement = admin_entity) %>% 
#    filter(nameDepartement != "PARIS") 
#
#data_dept$nameDepartement <- SameNameDept(data_dept$nameDepartement) 
#data_clim$nameDepartement <- SameNameDept(data_clim$nameDepartement) 
#expect_equal(setdiff(SameNameDept(data_dept$name_dept),SameNameDept(data_clim$nameDepartement)),character(0)) 
#data_clim <- merge(data_dept, data_clim, by="nameDepartement") 
#
#vars <- c("pr_", "etp_", "tx_", "tn_", "rv_", "tx34_", "tn17_", "tx_0_10_")
#newnames <- c("Precip_", "ETP_", "Tmax_", "Tmin_", "Ray_", "Tmax_ab34_", "Tmin_bel17_", "Tmax_bet010_")
#names_meteo <- data.frame(vars, newnames,stringsAsFactors = FALSE) 
#
#for (i in 1:8){
#colnames(data_clim) <- gsub(vars[i], newnames[i],colnames(data_clim))
#}
##dirty but will correct bug so far
#colnames(data_clim) <- gsub("Tmax_0_10_", "Tmax_bet010_", colnames(data_clim))


###FORMER DATA PREP (based on Data_BA_Rdt from Nathan) 
#
#cols_DP <- append(organismList$organism, c("code_dept", "periode")) 
#Dept_Pression <- subset( Data_rdt_ba,select = cols_DP)
#
#Dept_Pression <- melt(Dept_Pression, id=c("code_dept", "periode")) 
#Dept_Pression <- Dept_Pression %>% 
#    rename(codeDepartement = code_dept, campagne = periode, subject=variable , MedMoy_Dept_Same = value) %>% 
#    mutate(campagne = as.integer(campagne)) %>% 
#    mutate(codeDepartement = as.factor(as.character(codeDepartement))) 

    #Check result: all group present, thiw will change when 2017 will be added (cf make_Data_Dept 

#Making sure we have all combinations 
dat <- expand.grid(unique(dat_pression$subject), unique(dat_pression$codeDepartement), unique(dat_pression$campagne))
colnames(dat) <- c("subject", "codeDepartement", "campagne")
Dept_Pression <- full_join(dat, dat_pression)



allLinesDP <- Dept_Pression[,c("codeDepartement","campagne","subject")] %>% 
    arrange(subject, codeDepartement, campagne)
allYears <- 2009:2017
allLinesDP <- allLinesDP[which(allLinesDP$campagne %in% allYears),]
test1 <- outer(sort(unique(as.character(allLinesDP$codeDepartement))),
               sort(unique(as.character(allLinesDP$campagne))),FUN=paste)
test1 <- outer(as.vector(test1),sort(unique(as.character(allLinesDP$subject))),FUN=paste)
test2 <- paste(allLinesDP$codeDepartement,allLinesDP$campagne,allLinesDP$subject)
expect_length(setdiff(test1,test2),0)
expect_length(setdiff(test2,test1),0) 

#    #Check result: year are present for each departement once NA removed, needed because NA exclude in lmer
#checkyear  <- Dept_Pression %>%
#    group_by(codeDepartement, subject)  %>%
#    summarize(nyear = nlevels(as.factor(as.character(campagne)))) 
#yearmiss <- checkyear %>% filter(nyear < 6) 
#if (nrow(yearmiss) == 0) {print("years ok")} else{print("years missing")} 
#

## Create Yn-1 pressure column with lag
Dept_Pression <- Dept_Pression %>%
    group_by(subject,codeDepartement)  %>%  
    mutate(MedMoy_Dept_Ymin1 = lag(MedMoy_Dept_Same, order_by=campagne)) 
Dept_Pression <- Dept_Pression %>% arrange (subject, codeDepartement, campagne) 

    #Check coherence lag
allLinesDP <- Dept_Pression %>%
    mutate(org_dept = paste(codeDepartement, subject, sep="-")) 
CheckLaggedValue(allLinesDP$org_dept, allLinesDP$campagne, allLinesDP$MedMoy_Dept_Same, allLinesDP$MedMoy_Dept_Ymin1)

#     for (ind in unique(allLinesDP$org_dept)) { 
#           for (y in c(2010:2014)) {
#               i=which(allLinesDP$org_dept == ind & allLinesDP$campagne == y, arr.ind = T)
#               j=which(allLinesDP$org_dept == ind & allLinesDP$campagne == (y-1), arr.ind = T)
#               year = allLinesDP$MedMoy_Dept_Ymin1[i] 
#               prevyear = allLinesDP$MedMoy_Dept_Same[j]
#               if(year %nin%  prevyear) {cat(year, prevyear, "erreur lag")}
#           }
#       }

# Estimation of MedMoy_Dept_Same accounting for department effect 
require(lme4) 

    #Model per organism
pred_mod_pression <- NULL 
res_auto_mod_pression <- NULL
for (sub in unique(Dept_Pression$subject)){
 sub_Pression <- Dept_Pression %>% filter(subject == sub) 
 ran_mod_pression <- lmer(data = sub_Pression, MedMoy_Dept_Same ~ 1|codeDepartement, na.action = "na.exclude")
 pred_sub <- predict(ran_mod_pression)
 pred_mod_pression <- append(pred_mod_pression, pred_sub)
 res_sub <- residuals(ran_mod_pression)
 res_auto_mod_pression <-append(res_auto_mod_pression, res_sub)
}

    #check how are made residuals from residuals(lmer) 
res_mod_pression <- Dept_Pression$MedMoy_Dept_Same - pred_mod_pression 
#plot(res_auto_mod_pression, res_mod_pression) #x=y

    #bind res with data and apply lag on the residual to get dept. adjusted pressure
Dept_Pression <- data.frame(Dept_Pression, res_auto_mod_pression)
Dept_Pression <- Dept_Pression %>%
    rename(MedMoy_cor_Dept_Same= res_auto_mod_pression) %>% 
    group_by(subject,codeDepartement)  %>%  
    mutate(MedMoy_cor_Dept_Ymin1 = lag(MedMoy_cor_Dept_Same, order_by=campagne)) 

    #Check coherence lag
allLinesDP <- Dept_Pression %>%
        mutate(org_dept = paste(codeDepartement, subject, sep="-")) 
    CheckLaggedValue(allLinesDP$org_dept, allLinesDP$campagne, allLinesDP$MedMoy_cor_Dept_Same, allLinesDP$MedMoy_cor_Dept_Ymin1)

saveAs(Dept_Pression, "Dept_Pression") 

# Preparing data for parcell-year  observation merged with departemental pressure (obs/parcelle/year) and departement meteo 

## Merge with aOM and to make aOM_BAYmin1 
aOM_BAYmin1<- merge(aOM,Dept_Pression,  by=c("campagne", "subject", "codeDepartement"),all.x=TRUE)
expect_equal(nrow(aOM), nrow(aOM_BAYmin1))

#aOM_BAYmin1<- merge(aOM_BAYmin1, data_clim, by = c("campagne", "codeDepartement"), all.x = TRUE) 
#expect_equal(nrow(aOM), nrow(aOM_BAYmin1))

## column  Pressure * Surface culture Yn-1
iRPGPrec <- grep("RPGPrec_w.*_area", colnames(aOM_BAYmin1))
for (rpgprec in iRPGPrec){
    nomNewCol <- paste0("MedMoy_Dept_Ymin1_" , names(aOM_BAYmin1)[rpgprec])
    nomNewColcor <- paste0("MedMoy_cor_Dept_Ymin1_", names(aOM)[rpgprec])
    aOM_BAYmin1[, nomNewCol]<-aOM_BAYmin1[,rpgprec]*aOM_BAYmin1[,"MedMoy_Dept_Ymin1"]
    aOM_BAYmin1[, nomNewColcor]<-aOM_BAYmin1[,rpgprec]*aOM_BAYmin1[,"MedMoy_cor_Dept_Ymin1"]
}
names(aOM_BAYmin1) <- gsub(names(aOM_BAYmin1),pattern = "MedMoy_Dept_Ymin1_RPGPrec", replacement = "Prec_Dept_Ymin1")
names(aOM_BAYmin1) <- gsub(names(aOM_BAYmin1), pattern = "MedMoy_cor_Dept_Ymin1_RPGPrec", replacement = "Prec_cor_Dept_Ymin1")

#Need to remove NA from the la colum, we lose one year of data 

toRemove <- which(is.na(aOM_BAYmin1$MedMoy_Dept_Ymin1))
aOM_BAYmin1 <- aOM_BAYmin1[-toRemove,] 


save(aOM_BAYmin1, file="aOM_BAYmin1.rda")
#aOM <- aOM %>% select(-c("timeCreation", "timeMAJ", "timeSemis"))%>%  filter(campagne != 2009) 
#save(aOM, file = "aOM.rda") 


# Test on NA 
for (sub in unique(Dept_Pression$subject)) {
sub_pression <- Dept_Pression %>% filter(subject == sub)
subtable <-table(sub_pression$campagne,is.na(sub_pression$MedMoy_Dept_Same))#how much dept with no data
print(paste("=======",sub,"======="))
print(subtable)
 }
stop("continue if further check needed")


#### Test coherence NA departement and years

#on Dept_Pression (that contain all combination of year*dept*subjet) 
edit(Dept_Pression[which(is.na(Dept_Pression$MedMoy_Dept_Same) & Dept_Pression$subject=="septoriose"),])
edit(Dept_Pression[which(is.na(Dept_Pression$MedMoy_Dept_Same) | is.na(Dept_Pression$MedMoy_Ymin1)) & Dept_Pression$subject=="septoriose",])
edit(Dept_Pression[which(is.na(Dept_Pression$MedMoy_Dept_Same) & Dept_Pression$subject=="septoriose"& Dept_Pression$codeDepartement == 59 ),])

# on the merge 
edit(aOM_BAYmin1[which(is.na(aOM_BAYmin1$MedMoy_Dept_Same)),]) #no NA in the output 
edit(aOM_BAYmin1[which(is.na(aOM_BAYmin1$MedMoy_Dept_Same) & aOM_BAYmin1$campagne != 2009),]) #no NA in the lag other than 2009 

edit(aOM_BAYmin1[which(is.na(aOM_BAYmin1$MedMoy_Dept_Same) & aOM_BAYmin1$subject=="septoriose"),])
edit(aOM_BAYmin1[which(is.na(aOM_BAYmin1$MedMoy_Dept_Same) | is.na(aOM_BAYmin1$MedMoy_Ymin1)) & aOM_BAYmin1$subject=="septoriose",])
edit(aOM_BAYmin1[which(is.na(aOM_BAYmin1$MedMoy_Dept_Same) & aOM_BAYmin1$subject=="septoriose"& aOM_BAYmin1$codeDepartement == 59 ),])

