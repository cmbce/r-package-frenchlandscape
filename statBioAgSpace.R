library("frenchLandscape")
# test correlation between colza density and presence of meligethes
load("metricsMeligethe.rda") # obtenu grâce au script exampleStatBioAg.R dans le package bioagresseur 

load("coordsColza.rda") 
load("metrics2009_w200.rda")
colnames(metrics) <- c("RPG2009g5_w200_minDistance","RPG2009g5_w200_count","RPG2009g5_w200_perimeter","RPG2009g5_w200_area")
landMetrics <- cbind(coordinates(coords),metrics)

load("metrics2010_w200.rda")
colnames(metrics) <- c("RPG2010g5_w200_minDistance","RPG2010g5_w200_count","RPG2010g5_w200_perimeter","RPG2010g5_w200_area")
landMetrics <- data.frame(cbind(landMetrics,metrics))

coordinates(landMetrics) <- ~ x+y
landMetrics$coordAsString <- paste(coordinates(landMetrics)[,"x"],coordinates(landMetrics)[,"y"])

temp<-coordinates(metricsMeligethe)
metricsMeligethe$coordAsString <- paste(temp[,"long"],temp[,"lat"])

metricsMeligethe2 <- merge(metricsMeligethe,landMetrics,by="coordAsString",all.x=TRUE,all.y=FALSE)

plot(metricsMeligethe2$RPG2009g5_w200_area,metricsMeligethe2$MelNbP_CountAbove_2)
glm(MelNbP_CountAbove_2 ~ RPG2009g5_w200_area,data=metricsMeligethe2,family=poisson())
summary(glm(MelNbP_CountAbove_2 ~ RPG2009g5_w200_area,data=metricsMeligethe2,family=poisson()))

