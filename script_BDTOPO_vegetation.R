# Note: to reload the package once it has been reinstalled
#    you can use reload from devtools package, ex:
#    reload("frenchLandscape");source("script_BDTOPO_vegetation.R")

library("frenchLandscape")
library("rgdal")
library("rgeos")
library("plotGoogleMaps")
library("testthat")

initWorkDir <- getwd()

# Corentin
inDir <- file.path(GetDicoPath("carto_init"),"BDTOPO")
# outDir <- system.file("extdata",package="BDTOPO")
outDir <- file.path(GetDicoPath("BDTOPO","raw")) # to test

# si un département est réimporté, son élément dans la liste est remplacé par le nouveau
# la liste est de forme 
# list(2007=list(001=list(out,warn=...,err=...),
#                095=...),
#      2008=list(...))
outFile <- file.path(outDir,"/out_from_import.rda")
if(file.exists(outFile)){
  load(outFile)
}else{
  out <- list()
}
for(year in 2017){
  for(numDep in 1:95){
    toLaunch <- factory(function(){ImportBDTOPOVegetation(year,numDep,inDir,outDir,simplify=FALSE)})
    out[[as.character(year)]][[as.character(numDep)]] <- toLaunch()
    save(out,file=paste0(outDir,"/out_from_import.rda")) # here so that no loss if stopped manually
  }
}
