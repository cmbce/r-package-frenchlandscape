
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

library("frenchLandscape")
load(file.path(GetDicoPath("bioagresseurs"),"tousBioagresseurs","allVigiParcgps.rda"))
load("allOrganismMetrics.rda")

cults <- c("bleTH","colza","mais","pdt","orgeH")
campagnes <- 2009:2017

# 
table(allVigiParcgps$campagne)
# 2009 2010 2011 2012 2013 2014 2015 2016 2017 
# 1843 3729 3514 3540 4075 3831 3938 4000 4341 
#=> légère tendance à l'augmentation (nouvelles cultures?)

sel <- which(allVigiParcgps$cult %in% cults & allVigiParcgps$campagne %in% campagnes)
aVP <- allVigiParcgps[sel,]
aVP$cult <- factor(aVP$cult)
aVP$culture <- factor(aVP$culture)
# augmentation notable en 2013
table(aVP$campagne)
# 2009 2010 2011 2012 2013 2014 2015 2016 2017 
#  809 1492 1486 1524 2099 1991 2053 2052 2235 

a<-as.matrix(table(aVP$cult,aVP$modif_coord_gps,useNA="ifany"))
b <- data.frame("nogeoref"=a[,1],"georef"=a[,2])
b$Total <- b[,"nogeoref"]+b[,"georef"]
b[,"% georef"] <- round(b[,"georef"]/b$Total *100,2)
#=> % modif_gps

# % gps ok formellement

aVPgeo<- aVP[which(aVP$modif_coord_gps=="Oui" & !is.na(aVP$longitudeWGS84)),]
aVPgeo <- spatDataManagement::LongLatToSp(aVPgeo,LongLat=c("lat","long"))
aVPgeo$coordsAsString <- GetCoordsAsString(aVPgeo)

b[,"geoOK"] <- as.vector(table(aVPgeo$cult))


allOrganismMetrics$cult <- factor(allOrganismMetrics$cult)
allOrganismMetrics$coordsAsString_campagne <- paste0(allOrganismMetrics$coordsAsString,"_",
                                                     allOrganismMetrics$campagne)
colToKeep <- setdiff(names(allOrganismMetrics),"NObs")
aOMparc <- allOrganismMetrics[match(unique(allOrganismMetrics$coordsAsString_campagne),
                                    allOrganismMetrics$coordsAsString_campagne),colToKeep]
aOMparcNObs <- aggregate(allOrganismMetrics[,"NObs",drop=FALSE],
                         by=list(coordsAsString_campagne=allOrganismMetrics$coordsAsString_campagne),
                         sum,na.rm=TRUE)
aOMparc$NObs <- aOMparcNObs$NObs[match(aOMparc$coordsAsString_campagne,aOMparcNObs$coordsAsString_campagne)]

aOMparc <- aOMparc[which(aOMparc$NObs>0),]
aOMparc <- aOMparc[which(aOMparc$campagne %in% campagnes),]

b[,"ObsBA"] <- table(aOMparc$cult)

b <- b[,c("Total","georef","% georef","geoOK","ObsBA")]
b[,"% obs"] <- round(b[,"ObsBA"]/b[,"Total"]*100,2)

b[,"RPG OK"] <- table(aOMparc$cult,aOMparc$RPGwithin0)[,"TRUE"]
b[,"% RPG OK"] <- round(b[,"RPG OK"]/b[,"Total"]*100,2)

# met <- frenchLandscape::ReadMetrics()
print(b[,c("Total","% georef","% obs","% RPG OK")])
# screen.copy -> Vigi2009-2014.png

# année 
ba <- table(aOMparc$campagne,aOMparc$RPGwithin0)
pourc <- ba[,2]/(ba[,1]+ba[,2])

ba <- table(aOMparc$cult,aOMparc$RPGwithin0)

