library("frenchLandscape")
library("spatDataManagement")
library("rgdal")
library("rgeos")
### exploration préalable
# inDir <- file.path(GetDicoPath("carto_init"),"BDTOPO/2017/vegetation/BDTOPO_2-2_VEGETATION_SHP_LAMB93_D024_2017-03-31/BDTOPO/1_DONNEES_LIVRAISON_2017-04-00260/BDT_2-2_SHP_LAMB93_D024_ED171")
# outDir <- GetDicoPath("BDTOPO")
# # vegOrig <- readOGR(file.path(inDir,"ZONE_VEGETATION.SHP"),"ZONE_VEGETATION")
# # 
# # vegOrig$lin <- GetLinearity(vegOrig,byid=TRUE)
# # vegOrig$area <- gArea(vegOrig,byid=TRUE)
# # save(vegOrig,file="vegOrig.rda")
# load(file.path(outDir,"vegOrig.rda"))
# 
# plot(log(vegOrig$area),log(vegOrig$lin),pch="+",col=vegOrig$NATURE)
# legend("topright",levels(vegOrig$NATURE),col=1:length(levels(vegOrig$NATURE)),pch=19)
# abline(v=log(1))
# abline(v=log(100))
# abline(v=log(10000))
# abline(v=log(1000000))
# 
# # inDir <- "/media/5AE1EC8814E5040E/donnees_R/BDTOPO"
# # vegSimple <- gSimplify(vegOrig,tol=10,topologyPreserve=TRUE) # perte des données (@data) càd des 5 variables et de leurs valeurs
# # vegSimple <- SpatialPolygonsDataFrame(vegSimple,vegOrig@data) # récupération du @data
# # all(rownames(vegOrig@data) == rownames(vegSimple))
# # saveAs(vegSimple,"vegSimple10",inDir)
# # vegSimple <- gSimplify(vegOrig,tol=2,topologyPreserve=TRUE) # perte des données (@data) càd des 5 variables et de leurs valeurs
# # vegSimple <- SpatialPolygonsDataFrame(vegSimple,vegOrig@data) # récupération du @data
# # all(rownames(vegOrig@data) == rownames(vegSimple))
# # saveAs(vegSimple,"vegSimple2",inDir)
# vegSimple <- gSimplify(vegOrig,tol=1,topologyPreserve=TRUE) # perte des données (@data) càd des 5 variables et de leurs valeurs
# load(file.path(outDir,"vegSimple10.rda"))
# load(file.path(outDir,"vegSimple2.rda"))
# 
# vegSimple2$lin <- GetLinearity(vegSimple2,byid=TRUE)
# vegSimple2$area <- gArea(vegSimple2,byid=TRUE)
#  
# vegSimple10$lin <- GetLinearity(vegSimple10,byid=TRUE)
# vegSimple10$area <- gArea(vegSimple10,byid=TRUE)
# 
# par(mfcol=c(2,4))
# plot(log(vegOrig$area),log(vegSimple2$area),pch=".")
# abline(a=0,b=1)
# plot(log(vegOrig$area),log(vegSimple10$area),pch=".")
# abline(a=0,b=1)
# plot(log(vegOrig$lin),log(vegSimple2$lin),col=2+(vegOrig$area>100),pch=".")
# abline(a=0,b=1)
# plot(log(vegOrig$lin),log(vegSimple10$lin),col=2+(vegOrig$area>100),pch=".")
# abline(a=0,b=1)
# plotZCol(log(vegOrig$area),log(vegOrig$lin),pch=".",zCol=vegOrig$NATURE,xLegend="topright",
#          ylim=c(0,5),xlim=c(-5,20))
# plotZCol(log(vegSimple2$area),log(vegSimple2$lin),pch=".",zCol=vegSimple2$NATURE,xLegend="topright",
#          ylim=c(0,5),xlim=c(-5,20))
# plotZCol(log(vegSimple10$area),log(vegSimple10$lin),pch=".",zCol=vegSimple10$NATURE,xLegend="topright",
#          ylim=c(0,5),xlim=c(-5,20))
# 
# library("pryr")
# pryr::object_size(vegOrig)
# ngat
# # => 1.23 GB
# pryr::object_size(vegSimple2)
# # => 885 MB

#=> how comes they are respectively 197M and 87M ? 
#   dans les deux cax il y a un facteur de compression proche de 10, algorithme de compression défaillant ? 
#   en tout cas, même pour vegSimple1, la taille du rda est très réduite pour une perte tout à fait 
#   marginale de qualité, le temps de chargement n'est réduit que de 10% mais le véritable intérêt 
#   c'est que ça prends 30% de place en moins, en particulier sur Migale c'est important
#   => donc on pourrait refaire le calcul de linéarité à 1m

veg <- get(load(file.path(GetDicoPath("BDTOPO"),"raw","vegetation_2017_001.rda")))

system.time(a<-GetWithinOf(veg,1000,veg[1,]))
#=> 0.36s /0.63 with gDistanceByIdPar
# to be compared to (similar)
system.time(b <- veg[which(!is.na(over(veg,gBuffer(veg[1,],width=1000)))),])
#=> 1.85s
system.time(a<-GetWithinOf(GetCentroids(veg),1000,veg[1,]))

#=> doing better than the default but only factor 5

library("spatDataManagement")

plotWithinOf(veg,c(500,1000),veg[1000,],zcol="NATURE")
plotWithinOf(GetCentroids(veg),c(500,1000),veg[1000,],zcol="NATURE") 

# métrique d'élongation (elongation metric)
# measuring the maximal distance between two points of the perimeter
# TODO

# training on philly (smaller)
# just use the first polygon
GetInnerDist <- function(SP){
    if(!is.projected(SP)){
        SP <- Project(SP)
    }
    innerDist <- unlist(lapply(SP@polygons,function(x){
                                   out <- max(dist(x@Polygons[[1]]@coords))
                                   return(out)
}))
    return(innerDist)
}
system.time(phillyCensusTracts$inDist <- GetInnerDist(phillyCensusTracts))
mapview(phillyCensusTracts,zcol="inDist",legend=T)
#=> seems spot on
vegSamp <- veg[1:1000,]
library("rgeos")
vegSamp$area <- gArea(vegSamp,byid=TRUE)
system.time(vegSamp$inDist <- GetInnerDist(vegSamp))
vegSamp$elong <- pi*(vegSamp$inDist/2)^2/vegSamp$area
vegSamp$lin <- GetLinearity(vegSamp,byid=TRUE)
mapview(vegSamp,zcol="elong",legend=T)

par(mfrow=c(1,2))
plot(log(vegSamp$area),log(vegSamp$lin))
plot(log(vegSamp$area),log(vegSamp$elong))

plotZCol(log(vegSamp$area),log(vegSamp$lin),pch=19,zCol=vegSamp$NATURE,xLegend="topright",
         ylim=c(0,3),xlim=c(-5,20))
abline(v=log(1000))
plotZCol(log(vegSamp$area),log(vegSamp$elong),pch=19,zCol=vegSamp$NATURE,xLegend="topright",
         ylim=c(0,5),xlim=c(-5,20))
abline(v=log(1000))

bigHR <- vegSamp[which(vegSamp$NATURE=="bois"&vegSamp$area>1000),]
map <- plotGoogleMaps(bigHR,add=T,zcol="area")
bigHRLoc <- GetCentroids(bigHR)
map <- plotGoogleMaps(bigHRLoc,previousMap=map)
#=> en large majorité des parties de haies, dans quelques cas des arbres isolés
plotWithinOf(veg,1000,veg[which(veg$ID=="ZONEVEGE0000000221619434"),],zcol="NATURE")
#=> tout à fait typique: un morceau marqué en bois alors que c'est juste un morceau de haie, en continuité avec d'autres morceaux marqués comme haies
#   par contre c'est assez clair qu'il y a des haies qui manquent

veg <- get(load(file.path(GetDicoPath("BDTOPO"),"raw","vegetation_2017_024.rda")))
veg <- vegetation_2017_001
veg$lin <- GetLinearity(veg,byid=TRUE)
veg$area <- gArea(veg,byid=TRUE)

par(mfrow=c(1,2))
vegBois <- veg[which(veg$NATURE %in% c("bois","coniferes","feuillus","f.mixte","f.ouverte","haie","z.arboree")),]
plotZCol(vegBois$area,vegBois$lin,pch=19,zCol=factor(vegBois$NATURE),xLegend="topright",
         ylim=c(0,20),xlim=c(0,100000))
abline(a=1,b=0.00015)
abline(v=5000)
#=> bien mais plutôt 
vegBois$isole <- vegBois$area<5000 | (vegBois$lin-1)/vegBois$area>0.00015
plotZCol(vegBois$area,vegBois$lin,pch=19,zCol=factor(vegBois$isole),xLegend="topright",
         ylim=c(0,20),xlim=c(0,100000))
abline(a=1,b=0.00015)
abline(v=5000)

plotWithinOf(vegBois,1000,vegBois[1000,],zcol="isole",engine="plotGoogleMaps")
#=> not bad but clearly such chunks of "z.arboree" that have a lot of "should be isole" in them
#   => should definitely look at the area vs. the perimeter of this group
#   de plus il se pourrait bien que des vignes soient encore comptées dans "z.arboree"...


GetSmallWoods <- function(dat){
    col <- "NATURE"
    kept <- c("z.arboree","bois","feuillus","haie","coniferes","f.mixte","f.ouverte")
    small <- rgeos::gArea(dat,byid=TRUE)<1000
    return(dat[which(dat@data[,col] %in% kept & small),])
}
small <- GetSmallWoods(veg)
veg$area <- rgeos::gArea(veg,byid=TRUE)
plotWithinOf(veg,1000,veg[which(veg$ID=="ZONEVEGE0000000221652668"),],zcol="factNATURE",engine="mapview")
#=> c'est quand même pas joli la classif, même des peupleraies ont l'air de bois de feuillus 
#   normaux
peupl <- veg[which(veg$NATURE=="peupleraie"),]
#=> dans le département la plus part des peupleraies ont quand même bien l'air de peupleraies. La 
#   question est plutôt de savoir si on les garde ou pas ...



