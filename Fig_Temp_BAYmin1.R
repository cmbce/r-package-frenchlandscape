
########################
# 
# Script: Fig_Temp_BAYmin1.R   
#
# Purpose: Vizualize temporal variation of pest pressure from 2009 to 2017 and correlation with landscape 
# Details: Make annexes figures for the report using VigiObsto visualize within year periode of observation. Using Data_rdt_ba.rda to sutdy yearly and departemental pressure 
#
# Author: Thomas Delaune (thomas.delaune.inra@gmail.com)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)


library(dplyr) 
library(reshape2)
library(ggplot2)
library(frenchLandscape)
library(testthat)
library("spatDataManagement")
source("generalDataBAYmin1.R") 
source("statsFns.R")

load(file.path(GetDicoPath("pkgsFolder"),"r-package-frenchlandscape","aOM_BAYmin1.rda"))
load(file.path(GetDicoPath("pkgsFolder"),"r-package-frenchlandscape","Dept_Pression.rda"))
load(file.path(GetDicoPath("bioagresseurs"),"tousBioagresseurs/", "all_data_vigicultures_clean.rda")) 

outdir <- file.path(GetDicoPath("RPG"),"../..","communication","communication_tdelaune")
if(!file.exists(outdir)){
    dir.create(outdir,recursive=TRUE)
}

grid_ba_theme <- theme_classic() +  
    theme(axis.text.x = element_text(size=7),
          axis.text.y=element_text(size=7),
          axis.title = element_text(size = 8),
          axis.line = element_line(size=0.3),
          axis.ticks=element_line(size=0.3),         
          panel.spacing.x=unit(1, "lines"),
          panel.spacing.y=unit(1, "lines"),
          strip.background = element_blank(), 
          strip.text = element_text(size = 7, face = "bold"), 
          strip.placement= "inside")

'%nin%' <- Negate('%in%') 

#this function work if all the grouping factors are present in each timing factors 
CheckLaggedValue  <- function(groupvector, timevector,respvar, respvarlagged) {
       for (ind in unique(allLinesDP$org_dept)) { 
           for (y in c((min(unique(timevector))+1):max(unique(timevector)))) {
               i=which(groupvector == ind & timevector == y, arr.ind = T)
               j=which(groupvector == ind & timevector == (y-1), arr.ind = T)
               year = respvarlagged[i] 
               prevyear = respvar[j]
               if(year %nin%  prevyear){stop("the lag is not correct")}
           }
       }
}

#FIG. 1. Distribution temporelle des observations
#===============================================

# Preparing data for all observation (obs/parcelle/date obs)  

vigi_dat <- all_data_vigicultures_clean %>% 
    filter(code_obs %in%  organismList$elObs) %>%
    mutate(id_merge = paste(code_obs,cult, sep="-")) %>% 
    filter(!is.na(obs_date)) %>% 
    mutate(valeur = as.numeric(valeur)) %>% 
    filter(!is.na(valeur)) %>% 
    rename(culture_namefull = culture) 

organismList <- organismList %>% 
    mutate(id_merge = paste(elObs, culture, sep="-")) 

ba_obs_wyear <- merge(vigi_dat, organismList, by="id_merge") 
ba_obs_wyear$obs_date_intra <- format(as.Date(ba_obs_wyear$obs_date), "%m-%d") %>% as.Date(format("%m-%d"))
ba_obs_wyear <- ba_obs_wyear %>% 
    group_by(id_merge) %>% 
    mutate(scaled_valeur = AutoScale(valeur)) %>% 
    mutate(nobs_val = length(!is.na(valeur))) 

#  Period of the year when the observations are performed for each bioagressor, page/type 

for (class in unique(ba_obs_wyear$type)) {
    ba_obs <- ba_obs_wyear %>% filter(type == class)
    plot<- ggplot(data=ba_obs, aes(x=obs_date_intra))+
        geom_histogram(fill="dodgerblue3", color="black", size = 0.2, bins = 50)+
        scale_x_date(date_labels="%b", date_breaks = "2 months",expand=c(0,0), limits = as.Date(c("2019-01-01","2019-12-31")))+
        xlab("Periode d'observation")+
        grid_ba_theme+
        theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust=1))+
        facet_wrap(~subject,scales="free_x")+
        labs(title ="Distribution observations réalisées entre 2007 et 2017")+
        theme(plot.title=element_text(size = 10, face="bold", hjust = 0.5))
    ggsave(plot=plot, filename=paste0(class, "_histobs.pdf"), path = outdir)+
        graphics.off() 
}

##FIG. 1.5  Variation temporelle pression 
##=======================================
#
#for (class in unique(ba_obs_wyear$type)) {
#    ba_obs <- ba_obs_wyear %>% filter(type == class)
#    plot<- ggplot(data=ba_obs, aes(x=as.Date(obs_date), y=valeur))+
#        geom_line()+
#        scale_x_date(date_labels="%b-%y",expand=c(0,0))+
#        xlab("Periode d'observation")+
#        grid_ba_theme+ 
#        theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust=1))+
#        ylab("Pression bioagresseur (nombre de dépassement de seuil, nAboveMedMoy)")+
#        facet_wrap(~subject,scales="free_x")+
#        ggtitle("Variation temporelle de la pression bioagresseur en 2007 et 2017")+
#        theme(plot.title=element_text(size = 10, face="bold"))
#    ggsave(plot=plot, filename=paste0(class, "_vartemppression.pdf"), path = outdir)+
#        graphics.off() 
#}


##FIG. 1.5 Distribution spatiale des points  
##=======================================

ba_obs_wyear <- ba_obs_wyear %>% 
    filter(!is.na(long) & !is.na(lat) & codeDepartement != "2A" & codeDepartement != "2B" )

for (class in unique(ba_obs_wyear$type)) {
    ba_obs <-as.data.frame(ba_obs_wyear %>% filter(type == class)) %>%  
        mutate(lat = as.numeric(lat)) %>% 
        mutate(long = as.numeric(long)) %>% 
        filter(!is.na(lat) | !is.na(long)) 
    coordinates(ba_obs) <- ~long + lat 
    proj4string(ba_obs) <- CRS("+proj=longlat +ellps=WGS84")
    ba_obs <- spTransform(ba_obs, CRS(proj4string(frenchDepartements)))
    ba_obs <-unique(as.data.frame(cbind(ba_obs@data$subject, coordinates(ba_obs)))) 
    ba_obs <-ba_obs %>% rename(subject = V1)
    plot<- ggplot()+
        geom_polygon(data =frenchDepartements[which(! frenchDepartements$CODE_DEPT %in% c("2A","2B")),],
                     aes(x=long, y =lat, group = group), color = "black", fill ="white", size = 0.1)+ 
geom_point(data =ba_obs,aes(x=as.numeric(long), y=as.numeric(lat)), size = 0.1, alpha = 0.2,color = "dodgerblue3" )+

grid_ba_theme+theme_void()+
facet_wrap(~subject)+
labs(title ="Distribution spatiale des observations réalisées entre 2007 et 2017")+
theme(plot.title=element_text(size = 10, face="bold", hjust = 0.5))

 ggsave(plot=plot, filename=paste0(class, "_carto_u.pdf"), path = outdir)+
        graphics.off() 
}

#FIG. 2. Relation Pression N/N-1 
#===============================


#plots 
#-----------------------
organismList <- organismList %>% rename(fullname = subject, subject = organism) 
Dept_Pression <- inner_join(Dept_Pression, organismList)
# pression cor N/N-1 
for (class in unique(Dept_Pression$type)) {
    ba_obs <- Dept_Pression %>% filter(type == class)
    plot<- ggplot(data=ba_obs)+
        geom_point(aes(x=MedMoy_cor_Dept_Ymin1, y=MedMoy_cor_Dept_Same, size = sqrt(sum_NObs)),alpha = 0.3)+
        theme_classic() + 
        guides(size = FALSE)+
        scale_size_continuous(range = c(0.25, 3.5))+
        grid_ba_theme +
        xlab("Pression residuelle année n-1")+
        ylab("Pression residuelle année n")+
        geom_abline(linetype = "dashed", slope = 1, intercept = 0)+
        facet_wrap(~subject,scales="free_x")+
        ggtitle("Pression departementale de l'année n-1 sur l'année n,\n residus du modele Pression ~ 1|Departement")+
        theme(plot.title=element_text(size = 10, face="bold"))

    ggsave(plot = plot, filename = paste0(class, "_correldep_cor.pdf"), path = outdir) 
    graphics.off() 
# pression  N/N-1 
    plot<- ggplot(data=ba_obs)+
        geom_point(aes(x=MedMoy_Dept_Ymin1, y=MedMoy_Dept_Same, size = sqrt(sum_NObs)),alpha = 0.3)+ 
        theme_classic() + 
        guides(size = FALSE)+
        scale_size_continuous(range = c(0.25, 3.5))+
        grid_ba_theme +
        xlab("Pression année n-1")+
        ylab("Pression année n")+
        geom_abline(linetype = "dashed", slope = 1, intercept = 0)+
        facet_wrap(~subject,scales="free_x")+
        ggtitle("Pression départementale de l'année n-1 sur l'année n")+
        theme(plot.title=element_text(size = 10, face="bold"))

    ggsave(plot = plot, filename = paste0(class, "_correldep.pdf"), path = outdir) 
    graphics.off() 
}
# pression cor N-1 sur N 
    plot<- ggplot(data=ba_obs)+
        geom_point(aes(x=MedMoy_cor_Dept_Ymin1, y=MedMoy_Dept_Same, size = sqrt(NObstot)),alpha = 0.3)+ 
        theme_classic() + 
        guides(size = FALSE)+
        scale_size_continuous(range = c(0.25, 3.5))+
        grid_ba_theme +
        xlab("Pression année n-1")+
        ylab("Pression année n")+
        geom_abline(linetype = "dashed", slope = 1, intercept = 0)+
        facet_wrap(~subject,scales="free_x")+
        ggtitle("Pression départementale corrigée sur pression de l'année suivante non corrigée")+
        theme(plot.title=element_text(size = 10, face="bold"))

    ggsave(plot = plot, filename = paste0(class, "_coronly.pdf"), path = outdir) 
    graphics.off() 

    #plot both corrected and uncorrected version on the same graph 
#    plot<- ggplot(data=ba_obs)+
#        geom_point(aes(x=MedMoy_Dept_Ymin1, y=MedMoy_Dept_Same,size = sqrt(NObstot)), alpha = 0.3, color = "black")+ 
#        geom_point(aes(x=MedMoy_cor_Dept_Ymin1, y=MedMoy_cor_Dept_Same,size = sqrt(NObstot)), alpha=0.3, color="dark blue" )+ 
#        theme_classic() + 
#        guides(size = FALSE)+
#        scale_size_continuous(range = c(0.25, 3.5))+
#        grid_ba_theme+
#        xlab("Pression année n-1 ajustée (bleu) et non ajustée (noir)")+
#        ylab("Pression année n ajustée (bleu) et non ajustée (noir)")+
#        facet_wrap(~subject,scales="free_x")+
#        ggtitle("Relation entre la pression bioagresseur de l'année n-1 et l'année n")+
#        theme(plot.title=element_text(size = 10, face="bold"))
#
#    ggsave(plot = plot, filename = paste0(class, "_compare_correls2year.pdf"), path = outdir) 
#    graphics.off()
#
#    #plot relationship with lines for everydepartement, check cyclic pattern
#
#    plot<- ggplot(data=ba_obs)+
#        geom_line(aes(x=MedMoy_cor_Dept_Ymin1, y=MedMoy_cor_Dept_Same, group = codeDepartement), alpha = 0.3, color = "black")+ 
#        theme_classic() + 
#        grid_ba_theme+
#        xlab("Pression année n-1 ajustée")+
#        ylab("Pression année n ajustée")+
#        facet_wrap(~subject,scales="free_x")+
#        ggtitle("Relation entre la pression bioagresseur de l'année n-1 et l'année n")+
#        theme(plot.title=element_text(size = 10, face="bold"))
#
#    ggsave(plot = plot, filename = paste0(class, "_lines_correls2year.pdf"), path = outdir) 
#    graphics.off()

   #boxplot temporal

    plot<- ggplot(data=ba_obs)+
        geom_boxplot(aes(x=factor(campagne), y=MedMoy_cor_Dept_Same), fill = "dodgerblue3")+ 
        theme_classic() + 
        grid_ba_theme+
        xlab("Annee d'observation")+
        theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust=1))+
        ylab("Pression residuelle")+
        facet_wrap(~subject,scales="free_x")+
        ggtitle("Variations temporelles de la pression départementale")+
        theme(plot.title=element_text(size = 10, face="bold"))

    ggsave(plot = plot, filename = paste0(class, "_box_correls2year.pdf"), path = outdir) 
    graphics.off()

    ba_obs <- ba_obs %>% group_by(type, subject, campagne) %>% summarize(sumObs = sum(NObstot))
    plot<- ggplot(data=ba_obs, aes(x=factor(campagne), y=sumObs), fill = "dodgerblue3")+
        geom_bar(stat = "identity", color = "black", fill = "dodgerblue3", size=0.3)+ 
        theme_classic() + 
        grid_ba_theme+
        xlab("Annee")+
        theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust=1))+
        ylab("Nbr. d'observations")+
        facet_wrap(~subject,scales="free_x")+
        ggtitle("Variations inter-annuelles du nombre d'observation")+
        theme(plot.title=element_text(size = 10, face="bold"))

    ggsave(plot = plot, filename = paste0(class, "_bar_nbr_obs.pdf"), path = outdir) 
    graphics.off()


#plot<- ggplot(data=subset(Dept_Pression, subject == "Septoriose" & codeDepartement == "59" ),aes(x=MedMoy_cor_Dept_Ymin1, y=MedMoy_cor_Dept_Same))+
#    geom_point()+  
#    geom_text(aes(label = (campagne -1)))+
#    grid_ba_theme+
#    xlab("Pression année n-1 ajustée")+
#    ylab("Pression année n ajustée ajustée")+
#    ggtitle("Relation entre la pression bioagresseur de l'année n-1 et l'année n")+
#    theme(plot.title=element_text(size = 10, face="bold"))


#FIG. 3. Relation Pression/RPGprec interaction 
#=============================================
#
#typelist <- organismList %>%
#    select(organism, subject, type) %>% 
#    rename(organism = subject, subject = organism, type = type)
#aOM_BAYmin1 <- merge(aOM_BAYmin1, typelist, by ="subject")
#aOM_BAYmin1 <-aOM_BAYmin1 %>% select(-c(timeCreation, timeMAJ, timeSemis))
#
##Make grid of graph for each type
#for (class in unique(aOM_BAYmin1$type)) {
#    ba_aOM <- aOM_BAYmin1 %>% filter (type == class) 
#
#    #Pressure RPG plot (*PressureYn-1, Same et Prec) 
#    iRPGPrecMedMoy <-names(aOM_BAYmin1)[grep("RPGPrec_Dept_Ymin1_w.*_area", colnames(aOM_BAYmin1))]
#    for (scale in iRPGPrecMedMoy){
#        plot <- ggplot(data=ba_aOM, aes_string(x=scale, y="nAboveMedMoy"))+ 
#            geom_hex(bins=10)+ 
#            theme_classic()+ 
#            scale_fill_distiller(palette="Spectral")+
#            scale_x_continuous(labels=function(x)x/10000)+
#            facet_wrap(~organism, scales = "free_x") +
#            theme(axis.text.x = element_text(size=7, angle = 45, vjust = 1))+
#            grid_ba_theme+
#            ylab("Pression bioagresseur nAboveMedMoy")+
#            xlab("Pression n-1 (nAboveMedMoy) * RPGPrec (area)")+
#            ggtitle(paste0("Relation pression bioagresseur et interaction \n pression n-1 *",scale))+
#            theme(plot.title=element_text(size = 10, face="bold"))
#
#        ggsave(plot = plot, filename = paste0(class,scale, "_correlPrecMedMoy.pdf"), 
#               path = outdir) 
#        graphics.off() 
#    }
#}
#for (class in unique(aOM_BAYmin1$type)) {
#    ba_aOM <- aOM_BAYmin1 %>% filter (type == class) 
#
#    #Pressure RPG plot (*PressureYn-1, Same et Prec) 
#    iRPGPrecMedMoy <-names(aOM_BAYmin1)[grep("RPGPrec_cor_Dept_Ymin1_w.*_area", colnames(aOM_BAYmin1))]
#    for (scale in iRPGPrecMedMoy){
#        plot <- ggplot(data=ba_aOM, aes_string(x=scale, y="nAboveMedMoy"))+ 
#            geom_hex(bins=10)+ 
#            theme_classic()+ 
#            scale_fill_distiller(palette="Spectral")+
#            scale_x_continuous(labels=function(x)x/10000)+
#            facet_wrap(~organism, scales = "free_x") +
#            theme(axis.text.x = element_text(size=7, angle = 45, vjust = 1))+
#            grid_ba_theme+
#            ylab("Pression bioagresseur nAboveMedMoy")+
#            xlab("Pression n-1 (nAboveMedMoy)corrigé * RPGPrec (area)")+
#            ggtitle(paste0("Relation pression bioagresseur et interaction \n pression n-1 corrigé *",scale))+
#            theme(plot.title=element_text(size = 10, face="bold"))
#
#        ggsave(plot = plot, filename = paste0(class,scale, "_adj_correlPrecMedMoy.pdf"), 
#               path = outdir) 
#        graphics.off() 
#    }
#}
#

#==================================================
#for (class in unique(aOM_BAYmin1$type)) {
#    ba_aOM <- aOM_BAYmin1 %>% filter (type == class) 
#
#    iRPGPrec <-names(aOM_BAYmin1)[grep("RPGPrec_w.*_area", colnames(aOM_BAYmin1))]
#    for (scale in iRPGPrec){
#        plot <- ggplot(data=ba_aOM, aes_string(x=scale, y="nAboveMedMoy"))+ 
#            geom_hex(bins=10)+ 
#            theme_classic()+ 
#            scale_fill_distiller(palette="Spectral")+
#            scale_x_continuous(labels=function(x)x/10000)+
#            facet_wrap(~organism, scales = "free_x") +
#            theme(axis.text.x = element_text(size=7, angle = 45, vjust = 1))+
#            grid_ba_theme +
#            ylab("Pression bioagresseur nAboveMedMoy")+
#            xlab("RPGPrec (area)")+
#            ggtitle(paste0("Relation pression bioagresseur et\n ", scale))+
#            theme(plot.title=element_text(size = 10, face="bold"))
#        ggsave(plot = plot, filename = paste0(class,scale, "_correlPrec.pdf"),  
#               path = outdir) 
#        graphics.off() 
#    }
#}
#    #Pressure sum/proportion year boxplot 
#    dat <- ba_aOM %>% 
#        group_by(campagne, codeDepartement, organism) %>%
#        summarize(sumpression = sum(nAboveMedMoy, na.rm=T))
#    plot <- ggplot(data = dat, aes(x=factor(campagne) ,y=sumpression))+
#        geom_boxplot()+
#        theme_classic()+ 
#        facet_wrap(~organism) 
#    ggsave(plot = plot, filename = paste0(class, "_boxsumyear.png")) 
#    graphics.off() 
#
#    dat <- ba_aOM %>% 
#        group_by(campagne, codeDepartement, organism) %>%
#        summarize(proppression = sum(nAboveMedMoy, na.rm=T)/sum(NObs, na.rm=T))
#    plot <- ggplot(data = dat, aes(x=factor(campagne) ,y=proppression))+
#        geom_boxplot()+
#        theme_classic()+ 
#        facet_wrap(~organism) 
#    ggsave(plot = plot, filename = paste0(class, "_boxpropyear.png")) 
#    graphics.off() 
#
#    #Pressure year boxplot
#    plot <- ggplot(data = ba_aOM, aes(x=factor(campagne) ,y=nAboveMedMoy))+
#        geom_boxplot()+
#        theme_classic()+ 
#        facet_wrap(~organism) 
#    ggsave(plot = plot, filename = paste0(class, "_boxyear.png")) 
#    graphics.off() 
#

