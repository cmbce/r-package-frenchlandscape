
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

load("/datasets/donnees_R/Agreste/AGRESTE_2014.rda")
nat <- AGRESTE_2014[grep("00",AGRESTE_2014$CODE_REG),]
nat <- nat[order(nat$Area),]


studiedCrops <- c("ble_tendre","colza","mais_gr","orge","mais_ens","betterave","pomme_de_t")
allGrainCropsArea <- sum(nat$Area)
studiedGrainCropsArea <- sum(nat$Area[which(nat$ESPECE %in% studiedCrops)])



