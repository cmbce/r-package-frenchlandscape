source("~/r-packages/r-package-frenchlandscape/statsFns.R")

########################
# Import and merge data
########################
# métriques bioagresseur
ExtLoad(pkg = "bioagresseurs", "sclerotinia/sclerotiniaVigi", ext=".rda")->sclerotinia
ExtLoad(pkg = "bioagresseurs", "sclerotinia/sclerotinia_metrics", ext=".rda")->sclerotiniaMetrics
sclerotiniaMetrics <- sclerotiniaMetrics[which(sclerotiniaMetrics$Modif.Coord..GPS=="Oui"),]

LM <- ReadMetrics()
sclerotiniaMetrics <- ForceProjOfFirst(frenchDepartements,sclerotiniaMetrics)
LMclean <- LM[,-grep("RPG.*g1e",names(LM))]

sclerotiniaMetrics$coordsAsString <- GetCoordsAsString(sclerotiniaMetrics)
sclerotiniaMetrics  <- spatDataManagement::MergeToSpObject(spObj=sclerotiniaMetrics,df=LM,by="coordsAsString",all.sp=TRUE)

########################
# métriques composites 
########################
sclerotiniaMetrics <- MakeCompositeMetrics(sclerotiniaMetrics,"g5")

iRPGSame <- grep("RPGSame_w.*_area",colnames(sclerotiniaMetrics@data))
iRPGPrec <- grep("RPGPrec_w.*_area",colnames(sclerotiniaMetrics@data))
iBDTOPO2012noVeg <- grep("BDTOPO2012[^v].*_w.*_area",colnames(sclerotiniaMetrics@data))
iPrairiesSame <- grep("PrairiesSame.*_w.*_area",colnames(sclerotiniaMetrics@data))
iColReg <- c(iRPGSame,iRPGPrec,iBDTOPO2012noVeg,iPrairiesSame)

### identifie rangs avec des NA
rowReg <- which(apply(sclerotiniaMetrics@data[,iColReg],1,function(vect){all(!is.na(vect))}))

notOneNA <- apply(sclerotiniaMetrics@data[,iColReg],1,function(vect){all(!is.na(vect))})

### identifie rangs de régions ayant trop peu de points
# matFactors <- model.matrix(metricsMeligethe@data$MelNbP_CountAbove_2[rowReg]~metricsMeligethe@data[rowReg,"grande.région"]+Campagne)[,-1]
# aggregate(matFactors,by=list(matFactors[,"Campagne"]),sum)
#=> eliminate bretagne, est humide, poitou - façade atlantique, sud-bassin parisien
regionTooFew <- c("bretagne", "est humide", "poitou - façade atlantique", "sud-bassin parisien")
notTooFew <- ! sclerotiniaMetrics@data[,3016] %in% regionTooFew
# NB: la région Bassin parisien devient la région de référence

### retire indésirables
rowReg <- which(notOneNA & notTooFew & which(!is.na(sclerotiniaMetrics@data$sclerotiniaMetrics_CountAbove_10)))
 
### if want to control for years, need 
# Campagne <- metricsMeligethe@data[rowReg,"Campagne"]
# matFactors <- model.matrix(metricsMeligethe@data$MelNbP_CountAbove_2[rowReg]~metricsMeligethe@data[rowReg,"grande.région"]+Campagne)[,-1]
# for some reason lassoscore really doesn't like it...
### else
# rowReg <- which(notOneNA)
matFactors <- model.matrix(sclerotiniaMetrics@data$sclerotiniaMetrics_CountAbove_10[rowReg]~factor(sclerotiniaMetrics@data[rowReg,3016]))[,-1]
### fi

colnames(matFactors) <- gsub("^.*]","",colnames(matFactors))

y10 <- sclerotiniaMetrics@data$sclerotiniaMetrics_CountAbove_10[rowReg]
X <- as.matrix(data.frame(sclerotiniaMetrics@data[rowReg,iColReg],matFactors))

# and for glm
Xglm10 <- model.matrix(y10~X)

####################
# Model(s)
####################
#===========
# simple multi-variate glm
#===========
print(summary(glm(y10~Xglm10,family="poisson")))

#=> can see the "prairies" but not much more

#===========
# Lasso
#===========
# res2 <- LassoPval(y2,X) # par défault maintenant utilise lambda.1se, lambda plus économe en paramètres
# plot(res2@cv) # a good idea to understand how lambda can change quickly. exp(-3.7) est probablement mieux ici
#               # je pense qu'au final il faudra prendre à la main le meilleur lambda pour chaque seuil de chaque
#               # bioagresseur
# res2 <- LassoPval(y2,X,lambda=exp(-3.7))
# res2.min <- LassoPval(y2,X,lambda="lambda.min") # plus de paramètre (erreur de prédiction minimale)
# res5 <- LassoPval(y5,X) 

# probably the best default: find the elbow between min (usually flat) and 1se (possibly quite far)
#    find where the curve "picks-up" and uses this as the lambda
#res2.elbow <- LassoPval(y2,X,lambda="polynomial.elbow") 
#res5.elbow <- LassoPval(y5,X,lambda="polynomial.elbow") 

# NB: les p-values peuvent changent un peu d'une exécution à une autre, c'est dû aux lambda.min identifiés par la validation croisée (processus aléatoire) qui ne sont pas toujours les mêmes peut-être qu'il faudrait faire une moyenne des différentes p-values qu'on obtient mais ça peut attendre. Pour l'instant géré en multipliant les folds

# NB2: j'ai ajouté le coef sur variable normalisée pour avoir une idée de la taille de l'effet par rapport à la variabilité dans le paysage (les p-values sont les mêmes)

# NB3: pour accéder à D2/D2 cor on peut utiliser res2@D2 par exemple

# NB4: pour l'instant il faut s'en tenir je pense aux résultats de 
# res2 <- LassoPval(y2,X)
# avec les coefs "normalisés" et les p-values sand.conf pour être sûr de pas risquer de dire trop de bêtises 


## une autre manière de visualiser: classe simplement les variables par ordre d'apparition (du départ au modèle
## avec erreur minimum) le chiffre correspond au nombre de fois où il est sélectionné (le plus haut est la 
## variable la plus sélectionné donc à priori la plus fiable
#print(LassoTreeSummary(res2.elbow))
#print(LassoTreeSummary(res5.elbow))

# Note: provence is pretty good at getting some but not at getting much...

mod10 <- glmnet(y=y10,x=X,family="poisson")
XNorm <- apply(X,2,AutoScale)
mod10Norm <- glmnet(y=y10,x=XNorm,family="poisson")
X11()
billboard10 <- LassoTreeSummaryBis(mod10)
X11()
billboard10Norm <- LassoTreeSummaryBis(mod10Norm)

#=> garder les 20 meilleurs lambda paraît sélectionner assez sévèrement les 15 meilleurs variables
meanCoefs10 <- apply(mod10$beta[names(billboard10),1:20],1,mean)

meanCoefs10Norm <- apply(mod10Norm$beta[names(billboard10Norm),1:20],1,mean)

tableBillCoefs10 <- data.frame(cbind(billboard10,meanCoefs10,meanCoefs10Norm))
tableBillCoefs10

metricName <- c("RPGSame", "RPGPrec", "BDTOPO2012bois", "BDTOPO2012haie", "BDTOPO2012lande", "PrairiesSame")

outFolderPdf <- paste0(GetDicoPath("bioagresseurs"),"/sclerotinia/pdf")
for(num in 1:length(metricName))
{
              if(!file.exists(outFolderPdf)){dir.create(outFolderPdf)}
  pdf(file = paste0(outFolderPdf,"/sclerotiniaGrapheLasso_seuilHaut",metricName[num],".pdf"))
        #plot(0.5*c(-1,1),c(0,1),type="n",yaxt="n",xlab="meanCoefsNorm",ylab="",main=metricName[num]) # Visu perso avec les titres, pas fait pour rentrer dans un tableau
        plot(0.5*c(-1,1),c(0,1),type="n",yaxt="n",ann=F,main=metricName[num])
        abline(v=tableBillCoefs10[["meanCoefs10Norm"]][grep(metricName[num],rownames(tableBillCoefs10))[1]]) # 1 car on veut celui qui sort en premier
                    dev.off()
}



